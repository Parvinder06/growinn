import React, { Component } from 'react';
import { Router, Scene, Actions, ActionConst } from 'react-native-router-flux';
import { View, StyleSheet, Platform, Text } from 'react-native';
import { Color } from './src/utils/Color';
import { Fonts } from './src/utils/Constants';
import ExploreIc from './src/assets/images/icExplore.svgx'
import ExploreActiveIc from './src/assets/images/icExploreActive.svgx'
import UserIc from './src/assets/images/icUser.svgx'
import UserActiveIc from './src/assets/images/icUserActive.svgx'
import PurchaseIc from './src/assets/images/icPurchase.svgx'
import PurchaseActiveIc from './src/assets/images/icPurchaseActive.svgx'
import CartIc from './src/assets/images/icCart.svgx'
import CartActiveIc from './src/assets/images/icCartActive.svgx'
import { calcHeight, calcWidth } from './src/utils/Dimensions';

//Pre-login
import PreLogin from './src/PreLogin/index';
import MobileScreen from './src/PreLogin/MobileScreen/index';
import OtpScreen from './src/PreLogin/OtpScreen/index';
import Splash from './src/PreLogin/Splash';
import SetUpScreen from './src/PreLogin/SetUp';

//Home 
import Explore from './src/Home/Explore';

//Demo
import AgeScreen from './src/BookDemo/container/AgeScreen';
import SelectDemoCategory from './src/BookDemo/container/SelectDemoCategory';
import SelectDemoSubCat from './src/BookDemo/container/SelectDemoSubCat';
import Demo from './src/BookDemo/container/Demo';
import CartScreen from './src/Home/Cart';

//CourseDetail
import Course from './src/Course/container'

class routerComponent extends Component {
    TabIcon = ({ tabBarLabel, tintColor }) => {
        switch (tabBarLabel) {
            case 'Explore':
                return (
                    <View style={styles.iconContainer}>
                        {tintColor === 'rgb(43,115,204)' ? <ExploreActiveIc /> : <ExploreIc />}
                        <Text style={[styles.iconText, { color: `${tintColor}` }]}>{tabBarLabel}</Text>
                    </View>
                )
            case 'Purchase':
                return (
                    <View style={styles.iconContainer} >
                        {tintColor === 'rgb(43,115,204)' ? <PurchaseActiveIc /> : <PurchaseIc />}
                        <Text style={[styles.iconText, { color: `${tintColor}` }]}>{tabBarLabel}</Text>
                    </View>
                )
            case 'Profile':
                return (
                    <View style={styles.iconContainer} >
                        {tintColor === 'rgb(43,115,204)' ? <UserActiveIc /> : <UserIc />}
                        <Text style={[styles.iconText, { color: `${tintColor}` }]}>{tabBarLabel}</Text>
                    </View>
                )
            case 'My Cart':
                return (
                    <View style={styles.iconContainer} >
                        {tintColor === 'rgb(43,115,204)' ? <CartActiveIc /> : <CartIc />}
                        <Text style={[styles.iconText, { color: `${tintColor}` }]}>{tabBarLabel}</Text>
                    </View>
                )
            default: return null
        }
    };

    render() {
        return (
            <Router backAndroidHandler={() => null} >
                <Scene key='root'>
                    <Scene
                        key="splash"
                        component={Splash}
                        title='Splash'
                        hideNavBar
                        type="replace"
                    />

                    <Scene
                        key="preLogin"
                        component={PreLogin}
                        title='Login'
                        hideNavBar
                        type="replace"
                    // initial = {true}
                    // initial={this.props.welcome}
                    />

                    <Scene
                        key="mobileScreen"
                        component={MobileScreen}
                        title='MobileScreen'
                        hideNavBar
                        type="replace"
                    />
                    <Scene
                        key="otpScreen"
                        component={OtpScreen}
                        title='OtpScreen'
                        hideNavBar
                    //type="replace"
                    />
                    <Scene
                        key="setUpScreen"
                        component={SetUpScreen}
                        title='SetUpScreen'
                        hideNavBar
                        type="replace"
                    />
                    <Scene
                        key="ageScreen"
                        component={AgeScreen}
                        title='Age'
                        hideNavBar
                    />
                    <Scene
                        key="selectDemoCategory"
                        component={SelectDemoCategory}
                        title='Age'
                        hideNavBar
                    />
                    <Scene
                        key="selectDemoSubCat"
                        component={SelectDemoSubCat}
                        title='Age'
                        hideNavBar
                    />
                     <Scene
                        key="demo"
                        component={Demo}
                        title=''
                        hideNavBar
                    />
                    <Scene
                        key="course"
                        component={Course}
                        title=''
                        hideNavBar
                    />
                    <Scene
                        key='rootTab'
                        tabs
                        tabBarStyle={styles.tabBar}
                        tabStyle={{ borderColor: 'white' }}
                        hideNavBar
                        labelStyle={{ color: Color.appColor }}
                        showLabel={false}
                        activeTintColor={Color.appColor}
                        inactiveTintColor={Color.darkGreyText}
                        type={ActionConst.RESET}
                    >
                        <Scene tabBarLabel='Explore' icon={this.TabIcon} title='home' >
                            <Scene
                                key="explore"
                                component={Explore}
                                title='Home'
                                hideNavBar
                            //  type="replace"
                            />
                        </Scene>
                        <Scene tabBarLabel='My Cart' icon={this.TabIcon} title='cart' >
                            <Scene
                                key="cart"
                                component={CartScreen}
                                title=''
                                hideNavBar
                                type={ActionConst.REFRESH}
                               // onEnter={onEnterSomeView}
                               onEnter={()=>Actions.refresh()}
                            />
                        </Scene>

                    </Scene>


                </Scene>
            </Router>
        );
    }
}

const styles = StyleSheet.create({
    tabBar: {
        backgroundColor: Platform.OS === 'ios' ? 'white' : 'transparent',
        // height: Platform.OS === 'ios' ? calcHeight(5.5) : calcHeight(8.5),
        height: Platform.OS === 'ios' ? 50 : 70,
        justifyContent: 'center',
        alignItems: 'center',
        shadowRadius: 1,
        shadowOpacity: 0.1,
        shadowOffset: {
            width: 0,
            height: -5,
        },
        shadowColor: '#000000',
        elevation: 4,
        borderTopColor: 'transparent',
        borderTopWidth: 1,
    },
    iconContainer: {
        alignItems: 'center',
        alignSelf: 'center',
    },
    iconText: {
        textAlign: 'center',
        fontFamily: Fonts.sfMedium,
        fontSize: calcWidth(3)
    }
})

export default routerComponent;
