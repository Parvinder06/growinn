import { combineReducers } from 'redux';
import ToastViewReducer from './toastViewReducer';

export default combineReducers({
    toastViewActions: ToastViewReducer,
  });