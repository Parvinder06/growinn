import { ENABLE_TOASTVIEW, DISABLE_TOASTVIEW } from '../actions/actionTypes';

initialState = {
  isVisible: false,
  toastText: ''
};

const toastViewReducer = (state = initialState, action) => {
  switch (action.type) {
    case ENABLE_TOASTVIEW:
      return {
        ...state,
        isVisible: true,
        toastText: action.payload,
        messageType: action.messageType
      };
    case DISABLE_TOASTVIEW:
      return {
        ...state,
        toastText: '',
        isVisible: false
      };

    default:
      return state;
  }
};

export default toastViewReducer;
