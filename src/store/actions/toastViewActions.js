import { ENABLE_TOASTVIEW, DISABLE_TOASTVIEW } from './actionTypes';

export const enableToastView = (message, messageType) => {
  return {
    type: ENABLE_TOASTVIEW,
    payload: message,
    messageType
  };
};

export const disableToastView = () => {
  return {
    type: DISABLE_TOASTVIEW
  };
};
