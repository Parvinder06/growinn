import React, { Component } from 'react';
import { TouchableOpacity, View, Text } from 'react-native';
import { Fonts } from '../../utils/Constants';
import { calcHeight, calcWidth } from '../../utils/Dimensions'
import { Color } from '../../utils/Color';

// eslint-disable-next-line react/prefer-stateless-function
class LineButton extends Component {

  render() {
    return (
      <TouchableOpacity onPress={this.props.pressed} style={{
        height: this.props.height == null ? calcHeight(6) : this.props.height,
        width: this.props.width == null ? calcWidth(90) : this.props.width,
        justifyContent: 'center', borderRadius: 6,
        marginTop: this.props.marginTop ? this.props.marginTop : 20,
        borderWidth : this.props.line ? 1 : null,
        borderColor : this.props.line ? Color.lightGreen : null,
        // position: this.props.position == null ? null : this.props.position,
        alignSelf:  this.props.alignSelf ? this.props.alignSelf : 'center' ,
        backgroundColor: this.props.light  ? Color.appLight : (this.props.lightRed ? Color.lightRed :  this.props.line ? null : Color.lightGreen)
      }}
      >
        <View style={{
          height: this.props.height == null ? calcHeight(6) : this.props.height,
          width: this.props.width == null ? calcWidth(90) : this.props.width,
          justifyContent: 'center', borderRadius: 8,
          position: this.props.position == null ? null : this.props.position,
        }}
        >
          <Text style={{
            fontSize: this.props.fontSize ? this.props.fontSize : calcWidth(3),
            color: this.props.light || this.props.line ? Color.lightGreen  : (this.props.lightRed ? Color.red : 'white'),
            fontFamily: this.props.regular ? Fonts.gothamMedium : Fonts.gothamMedium,
            alignSelf: 'center',
          }}
          >{this.props.buttonText}</Text>
        </View>
      </TouchableOpacity>
    );

  }
};

export default LineButton;
