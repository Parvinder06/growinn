import React, { Component } from 'react';
import {
  View, StyleSheet, Dimensions, TouchableWithoutFeedback,
  Animated, Text
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import { Color } from '../../utils/Color';

import { disableToastView } from '../../store/actions/index';

class ToastView extends Component {
  constructor(props) {
    super(props);
    this.moveAnimation = new Animated.ValueXY({ x: 15, y: 0.01 })
  }

  moveToastView = () => {
    Animated.spring(this.moveAnimation, { toValue: { x: 15, y: 30 } }).start();
    setTimeout(() => {
      Animated.spring(this.moveAnimation, { toValue: { x: 15, y: -40 } }).start();
    }, 2899)
  }

  toastIndicator = () => {
    const toastColor = this.props.messageType ? this.props.messageType: Color.red;
    return {
      backgroundColor: toastColor,
      borderBottomLeftRadius: 10,
      borderTopLeftRadius: 10,
      width: 10,
      height: '100%'
    }
  }

  render() {

    if (!this.props.isVisible) {
      return null;
    }

      setTimeout(() => {
        this.props.disableToastView()
      }, 3000)

    return (
      <Animated.View style={[styles.container, this.moveAnimation.getLayout()]}>
        {this.moveToastView()}
        <View style={this.toastIndicator()} />
        <View style={styles.progressText}>
          <Text style={styles.codePushText}>{this.props.toastText}</Text>
        </View>
        <TouchableWithoutFeedback
          onPress={() => {
            this.props.disableToastView()
          }}
          style={{ alignSelf: 'center' }}
        >
          <Icon
            name='ios-close-circle-outline'
            size={40}
            color={this.props.messageType ? this.props.messageType : Color.red}
            style={{ marginRight: 10, alignSelf: 'center', marginTop: 5 }}
          />
        </TouchableWithoutFeedback>
      </Animated.View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(0,0,0,0.8)',
    width: Dimensions.get('window').width - 30,
    justifyContent: 'space-between',
    alignItems: 'center',
    alignSelf: 'center',
    shadowColor: '#000000',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    borderRadius: 10,
    marginBottom: 10,
    flexDirection: 'row',
    position: 'absolute',
    top: 10
  },
  progressBarContainer: {
    marginRight: 40
  },
  progressText: {
    paddingRight: 0,
    width: '80%',
    alignItems: 'flex-start'
  },
  button: {
    marginRight: 10,
    width: 70,
    alignSelf: 'center',
    height: 30
  },

  codePushText: {
    color: 'white',
    fontSize: 12,
    padding: 5,
    marginLeft: 0,
    textAlign: 'left'
  },
})

const mapStateToProps = (state) => {

  const {
    isVisible,
    toastText,
    messageType
  } = state.toastViewActions;

  return {
    isVisible,
    toastText,
    messageType
  };
}

export default connect(mapStateToProps, { disableToastView })(ToastView);
