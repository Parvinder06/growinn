import React, { Component, useState, useEffect } from 'react';
import { View, Image, FlatList, TouchableOpacity, Text, StyleSheet, BackHandler, Linking, Platform } from 'react-native';
import { calcHeight, calcWidth } from '../../utils/Dimensions'
import { Actions } from 'react-native-router-flux';
import { Color } from '../../utils/Color';
import BackOrange from '../../assets/images/BackOrange.svgx';

export const BackHeader = (props) => {


    return (
        <View style={styles.container}>
            <View style={styles.top} />
            <TouchableOpacity style={styles.white} onPress={() => Actions.pop()}>
                <BackOrange width={30} height={30} />
            </TouchableOpacity>
        </View>
    );

};

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        width: calcWidth(100),
        height: calcHeight(12)
    },
    top: {
        backgroundColor: Color.appThemeColor,
        width: calcWidth(100),
        height: calcHeight(5)
    },
    white: {
        backgroundColor: 'white',
        width: calcWidth(100),
        height: calcHeight(7),
        justifyContent: 'center',
        paddingHorizontal: 10
    }
})