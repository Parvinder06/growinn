import React, { Component, useState, useEffect } from 'react';
import { View, Image, FlatList, TouchableOpacity, Text, StyleSheet, BackHandler, Linking, Platform } from 'react-native';
import { calcHeight, calcWidth } from '../../utils/Dimensions'
import { Actions } from 'react-native-router-flux';
import { Color } from '../../utils/Color';
import BackBlack from '../../assets/images/BackBlack.svgx';
import { Fonts } from '../../utils/Constants';

export const BackHeader = (props) => {


    return (
        <View style={styles.container}>
            <View style={styles.top} />
            <View style = {styles.row}>
                { !props.backDisabled ? <View style ={styles.white}/> :
                <TouchableOpacity style={styles.white} onPress={() => Actions.pop()}>
                    <BackBlack width={18} height={18} />
                </TouchableOpacity> }
                <Text style ={styles.text}>{props.title}</Text>
                <View style ={styles.white}/>
            </View>
        </View>
    );

};

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        width: calcWidth(100),
        height: calcHeight(12),
    },
    top: {
        backgroundColor: Color.appThemeColor,
        width: calcWidth(100),
        height: calcHeight(5)
    },
    white: {
        backgroundColor: 'white',
        width: calcWidth(10),
        height: calcHeight(7),
        justifyContent: 'center',
        paddingHorizontal: 10
    },
    row:{
        flexDirection:'row',
        width: calcWidth(100),
        justifyContent:'space-between',
        alignItems:'center'
    },
    text :{
        textAlign: 'center',
        color: Color.charcoal,
        fontSize: calcWidth(4.5),
        fontFamily: Fonts.gothamMedium,
    }
})