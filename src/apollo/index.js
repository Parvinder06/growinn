//import { ApolloClient, InMemoryCache, from, HttpLink } from '@apollo/client';
import {ApolloClient, createHttpLink} from "@apollo/client";
import {setContext} from "@apollo/client/link/context";
import {InMemoryCache} from "@apollo/client";

import AsyncStorage from '@react-native-community/async-storage';
import DataManager from '../../src/database/DataManager';

const GRAPHQL_ENDPOINT =
    'http://13.127.32.139:4000/';

const apolloClient = () => {
   /*  let token = DataManager.getToken()
    const link = new HttpLink({
        uri: GRAPHQL_ENDPOINT,
        headers: {
            access_token: 'bearer '+token,
        },
    }); */ 


    const apolloHttpLink = createHttpLink({
        uri: GRAPHQL_ENDPOINT,
    }) 
    
    const apolloAuthContext = setContext(async (_, {headers}) => {
        const jwt_token = await AsyncStorage.getItem('Token_Key')
        return {
            headers: {
                ...headers,
                Authorization: jwt_token ? `Bearer ${jwt_token}` : ''
            },
        }
    })

    return new ApolloClient({
        link: apolloAuthContext.concat(apolloHttpLink),
        cache: new InMemoryCache(),
    });
};
export default apolloClient;

/* 
import {ApolloClient, createHttpLink} from "@apollo/client";
import {setContext} from "@apollo/client/link/context";
import {InMemoryCache} from "@apollo/client";

const apolloHttpLink = createHttpLink({
    uri: GRAPHQL_ENDPOINT,
})

const apolloAuthContext = setContext(async (_, {headers}) => {
    const jwt_token = localStorage.getItem('jwt_token')
    return {
        headers: {
            ...headers,
            Authorization: jwt_token ? `Bearer ${jwt_token}` : ''
        },
    }
})

export const apolloClient = new ApolloClient({
    link: apolloAuthContext.concat(apolloHttpLink),
    cache: new InMemoryCache(),
})
*/