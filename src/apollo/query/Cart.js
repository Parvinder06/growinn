
import gql from "graphql-tag";

export const GET_CART = gql`
{
    cartItem {
        id
        type
        category {
          id
    }    
        status
        slots
        temporaryId
        user {
          id
          name
          phoneNumber
        }
      }
  }
`;