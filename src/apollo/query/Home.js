import gql from "graphql-tag";

export const GET_CATEGORIES = gql`
{
    categories{
        id
        name
        webIcon   
        webImage
        subCategories{
          id
          name
          webImage
          appImage
          demoPrice
          tagLine
        }
      }
  }
`;

export const GET_COURSECONNECTION = gql`
query coursesConnection($first: Int!,$after: String){
  coursesConnection(first: $first, after: $after) {
    pageInfo{
      hasNextPage
      hasPreviousPage
      startCursor
      endCursor
    }
      edges{
        node{
          id
          name
          thumbnail
          rating
          demoVideoLink
          demoPrice
        shortDescription
        fullDescription
        category {
          name
        }
        teacher{
          name
        }
        requirements{
          description
        }
        levels{
          levelName
        }
        includes{
          description
        }
        }
      }
    }
  }
`;