import gql from "graphql-tag";

export const VERIFY_OTP = gql`
query VerifyOtp($id: ID!, $otp: String!) {
    verifyOtp (where: {
    id: $id },
    otp: $otp  )
  {
    id
    countryCode
    phoneNumber
    verified
  }
}
`;