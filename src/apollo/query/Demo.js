import gql from "graphql-tag";

export const GET_AGE = gql`
{
    ageGroups {
        id
        startAge
        endAge
    }
  }
`;

export const GET_TIME_SLOTS = gql`
{
  demoSlots {
      id
      startTime
      endTime
      startTimeInSec
      endTimeInSec
    }
  }
`;