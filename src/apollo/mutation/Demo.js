import gql from "graphql-tag";

export const ADD_DEMO_TO_CART = gql`
mutation addDemoToCart($type: String!, $cId: ID!,$cName: String!, $sId: ID!,$slots: [DateTime]!) {
    addDemoToCart (data: {
        type: $type
        category: {
            id: $cId,
                name : $cName
        }
        subCategory: {
            id: $sId
        }
    })
    {
        id
        type
        category
        subCategory
        status
        slots
        user
    }
    }
`;
