import gql from "graphql-tag";

export const SEND_OTP = gql`
mutation SendOtpForRegistration($countryCode: String!, $phoneNumber: String!) {
  sendOtpForRegistration (data: {
    countryCode: $countryCode
    phoneNumber: $phoneNumber  
  })
  {
    id
    countryCode
    phoneNumber
    verified
  }
}
`;

export const CREATE_USER = gql`
mutation createUser($name: String!, $countryCode: String!,$phoneNumber: String!, $email: String!,$role: Role!) {
  createUser (data: {
    name: $name
    countryCode: $countryCode  
    phoneNumber: $phoneNumber
    email: $email  
    role: $role  
  })
  {
    token
    user{
      name
    }
  }
}
`;

