import AsyncStorage from '@react-native-community/async-storage';
//import store from '../store/store';
//import { RESET_USER } from '../LoginModule/Actions/types'
class DataManager {

    constructor() {
        if (this.instance) {
            return this.instance;
        }
        this.instance = this;
        this.userProfileKey = "User_Profile_Key";
        this.tokenKey = "Token_Key";
        this.fcmToken = "fcm_token";
        this.initialScreen = "initial_screen_key";
        this.sessionId = "session_Id";
        this.ageGroup = "age_group";
        this.profileData = "profile_data";
    }

    getUserprofile = async () => {
        try {
            const value = await AsyncStorage.getItem(this.userProfileKey);
            if (value !== null) {
                return value;
            }
            return null;
        } catch (error) {
            throw (error);
        }
    }

    getFcmToken = async () => {
        try {
            const value = await AsyncStorage.getItem(this.fcmToken);
            if (value !== null && value !== '') {
                return value;
            }
            return null;
        } catch (error) {
            throw (error);
        }
    }

    resetUser = async () => {
       /*  try {
            this.setToken('');
            this.sessionId('');
            this.setInitialScreen('welcome')
            store.dispatch({ type: RESET_USER });
            return true;
        }
        catch (error) {
            throw (error);
        } */
    }

    setToken = async (token) => {
        try {
            const data = await AsyncStorage.setItem(this.tokenKey, token);
            console.log(data);
            return data;
        } catch (error) {
            throw (error);
        }
    }

    setSessionId = async (sessionId) => {
        try {
            const data = await AsyncStorage.setItem(this.sessionId, sessionId);
            console.log(data);
            return data;
        } catch (error) {
            throw (error);
        }
    }

    getSessionId = async () => {
        try {
            return await AsyncStorage.getItem(this.sessionId);
        } catch (error) {
            throw (error);
        }
    }


    getToken = async () => {
        try {
            AsyncStorage.getItem(this.tokenKey,(err,item) => {
                if (item) {
                    return item
                }
            });
        } catch (error) {
            throw (error);
        }
    }

    setInitialScreen = async (screen) => {
        try {
            return await AsyncStorage.setItem(this.initialScreen, screen);
        } catch (error) {
            throw (error);
        }
    }

    getInitialScreen = async () => {
        try {
            return await AsyncStorage.getItem(this.initialScreen);
        } catch (error) {
            throw (error);
        }
    }

    setAgeGroup = async (object) => {
        try {
            return await AsyncStorage.setItem(this.ageGroup, JSON.stringify(object))
        } catch (error) {
            throw (error);
        }
    }

    getAgeGroup = async () => {
        try {
            let value = await AsyncStorage.getItem(this.ageGroup);
            return JSON.parse(value)
        } catch (error) {
            throw (error);
        }
    }

    getInitialScreen = async () => {
        try {
            return await AsyncStorage.getItem(this.initialScreen);
        } catch (error) {
            throw (error);
        }
    }

    setProfileData = async (object) => {
        try {
            return await AsyncStorage.setItem(this.profileData, JSON.stringify(object))
        } catch (error) {
            throw (error);
        }
    }

    getProfileData = async () => {
        try {
            let value = await AsyncStorage.getItem(this.profileData);
            return JSON.parse(value)
        } catch (error) {
            throw (error);
        }
    }

}

export default new DataManager();
