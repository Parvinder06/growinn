import React, { Component, useState, useEffect } from 'react';
import { View,Text} from 'react-native';
import { styles } from './../Styles';

export const AgeTopView = (props) => {

    return (
        <View style={styles.containerAge}>
            <Text style ={styles.ageTitle}>{props.title}</Text>
            <Text style ={styles.ageSubTitle}>{props.subTitle}</Text>
        </View>
    );

};

export default AgeTopView;
