import React, { Component } from 'react';
import { View, TouchableOpacity, Text, FlatList, TouchableWithoutFeedback } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import { calcHeight, calcWidth } from '../../utils/Dimensions';
import { Color } from '../../utils/Color';
import { Fonts } from '../../utils/Constants';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import FastImage from 'react-native-fast-image';
import _ from "lodash";
import moment from 'moment';
import { styles } from './../Styles/calendarStyles';
import Modal from 'react-native-modal';
import CrossIc from '../../assets/images/icCloseDark.svgx';
import ViewPager from '@react-native-community/viewpager';
import { ScrollView } from 'react-native-gesture-handler';
import IcBackDisabled from '../../assets/images/backLight.svgx';
import IcBack from '../../assets/images/previousDark.svgx';
import IcNext from '../../assets/images/nextDark.svgx';
class Calendar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isModalVisible: true,
            paymentTypeVal: '',
            activeSlide: 0,
            days: [],
            stepIndex: 0,
            currentItem: 0,
            page: 0,
            selectedTimeSlots: [],
            timeSlots: props.timeSlots?.demoSlots,//this.props.allTimeSlots.filter(n => n.activeForToday) ,
            allTimeSlots: props.timeSlots?.demoSlots,//this.props?.allTimeSlots,
            selectedDate: moment(moment(), "DD-MM-YYYY").add(1, 'days')
        }
        this.currentStepIndex = 0;
        this.nextStep = this._nextStep.bind(this);
        this.previousStep = this._previousStep.bind(this);
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        let { allTimeSlots } = prevState;
        if (allTimeSlots !== nextProps.timeSlots) {
            allTimeSlots = nextProps.timeSlots?.demoSlots;
            timeSlots = nextProps.timeSlots?.demoSlots;
        }
        return { allTimeSlots }
    }

    dates = () => {
        let days = [];
        let startdate = moment()
        //  days.push(moment(startdate, "DD-MM-YYYY"))
        for (let index = 2; index <= 100; index++) {
            var day = moment(startdate, "DD-MM-YYYY").add(index, 'days');
            days.push(day);
        }
        this.setState({ dates: days });
    }
    componentDidMount() {
        this.dates()
    }
    getDay = (date) => {
        switch (date) {
            case 1:
                return 'SUN';
            case 2:
                return 'MON';
            case 3:
                return 'TUE';
            case 4:
                return 'WED';
            case 5:
                return 'THU';
            case 6:
                return 'FRI';
            case 0:
                return 'SAT';
        }
    }

    addItem = async (item) => {
        let data = this.state.selectedTimeSlots;
        let {selectedDate} = this.state;
        if (data.length < 2) {
            let selectedItem  = {
                id: item.id,
                startTime : item.startTime,
                endTime : item.endTime,
                startTimeInSec: moment(selectedDate).add(item.startTimeInSec, 'seconds') ,
                endTimeInSec:moment(selectedDate).add(item.endTimeInSec, 'seconds') ,
            }
            data.push(selectedItem)
            await this.setState({
                selectedTimeSlots: data
            })
            this.props.onPressSlots(data, this.state.selectedDate)
        }
        else {
            alert('Only two slots allowed')
        }

    }

    onClickTimeSlot = (items) => {
        let selected = this.state.selectedTimeSlots.some(ele => ele.id === items.id);
        if (selected) {
            this.deleteItem(items)
        }
        else {
            this.addItem(items)
        }

    }

    deleteItem = async (item) => {
        let data = this.state.selectedTimeSlots;

        var index = data.findIndex(function (o) {
            return o.id === item.id;
        })
        if (index !== -1) data.splice(index, 1);
        await this.setState({
            selectedTimeSlots: data
        })
        this.props.onPressSlots(data,this.state.selectedDate)
    }

    bookDemo = () => {
        /*         let item = {
                    courseId: this.props.item.id,
                    type: "demo",
                    paymentType: "one_time",
                    // subscriptionFrequency: "one_time",
                    demoTimeId1: this.state.selectedTimeSlots[0].id,
                    demoTimeId2: this.state.selectedTimeSlots[1].id,
                    date: moment(this.state.selectedDate).format('YYYY-MM-DD')
                }
                this.props.bookDemo(item) */
    }

    changeDate = async (item, index) => {
        let { timeSlots, allTimeSlots } = this.state;
        let day = this.getDay(moment(item).day());
        let today = this.getDay(moment().day())
        this.setState({ currentItem: index, selectedDate: item })
        if (day === today) {
            var todaySlots = allTimeSlots.filter(n => n.activeForToday);
            await this.setState({ timeSlots: [], selectedTimeSlots: [] })
            await this.setState({ timeSlots: todaySlots })
        }
        else {
            await this.setState({ timeSlots: [], selectedTimeSlots: [] })
            await this.setState({ timeSlots: allTimeSlots })
        }

    }

    renderDaysItem = (item, index) => {
        let { currentItem, selectedDate } = this.state;
        let day = this.getDay(moment(item).day());
        let date = moment(item).format('DD');
        let month = (moment(item).format('MMM'));
        return (
            <TouchableOpacity
                onPress={() => this.changeDate(item, index)}
                style={
                    [styles.dayFlatlist, { borderWidth: currentItem === index ? 0 : 1, borderColor: currentItem === index ? Color.appColor : '#e1e1e1', backgroundColor: currentItem === index ? Color.lightGreen : 'transparent' }]
                }>
                <Text style={[styles.authorText, {
                    color: currentItem === index ? 'white' : '#858585',
                    fontSize: calcWidth(2), marginTop: 5,
                }]}>{day}</Text>
                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                    <Text style={[styles.authorText, {
                        color: Color.blackText, color: currentItem === index ? 'white' : '#333333',
                    }]}>{date}{' '}</Text>
                    <Text style={[styles.authorText, {
                        color: Color.blackText, color: currentItem === index ? 'white' : '#333333',
                    }]}>{month}</Text>
                </View>
            </TouchableOpacity>

        )
    }

    renderTimeSlots = (item, index) => {
        let { currentItem } = this.state;
        let day = this.getDay(moment(item).day());
        let date = moment(item).format('DD');
        let month = (moment(item).format('MMM')).toUpperCase();
        let selected = this.state.selectedTimeSlots.some(ele => ele.id === item.id);
        // let selected =  this.state.selectedTimeSlots.filter((it) => it.id === item.id);
        return (
            <TouchableOpacity onPress={() => this.onClickTimeSlot(item)}
                style={[styles.button, {
                    width: calcWidth(30), height: calcHeight(3.8),
                    backgroundColor: selected ? Color.lightGreen : 'transparent', paddingHorizontal: 2, borderWidth: selected ? null : 1
                }]}>
                <Text
                    style={[styles.buttonText,
                    { color: selected ? 'white' : Color.blackText }]}
                >{item.startTime} - {item.endTime}</Text>
            </TouchableOpacity>
        )
    }

    render() {
        const { isModalVisible, paymentTypeVal, selectedTimeSlots } = this.state;
        const { duration, allTimeSlots } = this.props;
        return (
            <View
                style={styles.containerCalendar}
            >
                <Text style={styles.titleDemo}>{`Book a demo live dancing class`}</Text>
                <Text style={styles.subTitleDemo}>{'Select 30 minutes demo slot'}</Text>
                <View>
                    <View style={{
                        alignItems: 'center',
                        alignContent: 'center',
                        alignSelf: 'center',
                        flexDirection: 'row',
                        justifyContent: 'center',
                        marginTop: 10,
                    }}>
                        {this.state.stepIndex !== 0 ?
                            <TouchableOpacity
                                onPress={() => {
                                    this._previousStep()
                                }}
                                style={{ alignSelf: 'center' }}
                            >
                                <IcBack width={15} height={15} />
                            </TouchableOpacity> : <IcBackDisabled width={15} height={15} />
                        }

                        <View style={{
                            width: calcWidth(70), marginHorizontal: calcWidth(3), alignSelf: 'center', justifyContent: 'center',
                            alignItems: 'center', alignContent: 'center'
                        }}>
                            <FlatList
                                initialNumToRender={4}
                                scrollEnabled={false}
                                ref={(ref) => { this.flatListRef = ref; }}
                                maxToRenderPerBatch={4}
                                data={this.state.dates}
                                horizontal
                                renderItem={({ item, index }) => this.renderDaysItem(item, index)}
                            />
                        </View>

                        <TouchableOpacity
                            onPress={() => {
                                this._nextStep()
                            }}
                            style={{ alignSelf: 'center' }}
                        >
                            <IcNext width={15} height={15} />
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.subTitleSlotDemo}>{'Select any two time (GMT+5:30) slots when you avialable'}</Text>

                    <View style={{ width: calcWidth(90), maxHeight: calcHeight(15), alignItems: 'center', alignSelf: 'center', marginTop: 10 }}>

                        <ScrollView>
                            <FlatList
                                data={this.state.allTimeSlots}
                                numColumns={2}
                                renderItem={({ item, index }) => this.renderTimeSlots(item, index)}
                            />
                        </ScrollView>

                    </View>

                    <View style={{ flexDirection: 'row', alignSelf: 'center', marginTop: calcHeight(1.2) }}>
                        <Text style={[styles.slotfillingText, {
                            color: Color.orange,
                        }]}>{`• Hurry up! `}</Text>
                        <Text style={[styles.slotfillingText, {
                        }]}>{` Slots are filling fast`}</Text>
                    </View>
                </View>
            </View>
        );
    }

    _nextStep() {

        if (this.currentStepIndex < this.state.dates.length - 1) {
            this.currentStepIndex = this.currentStepIndex + 1;
            this.flatListRef.scrollToIndex({ index: this.currentStepIndex, animated: true });

            this.setState(
                {
                    stepIndex: this.currentStepIndex
                }
            )

        } else {
        }
    }

    _previousStep() {
        console.log('prev tapped...')
        if (this.currentStepIndex > 0) {
            this.currentStepIndex = this.currentStepIndex - 1;
            this.flatListRef.scrollToIndex({ index: this.currentStepIndex, animated: true });
            this.setState(
                {
                    stepIndex: this.currentStepIndex
                }
            )
        }
    }
}
export default Calendar;
