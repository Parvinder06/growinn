import { StyleSheet } from 'react-native';
import { calcHeight, calcWidth, calcHeightUsingPixel } from '../../utils/Dimensions'
import { Color } from '../../utils/Color';
import { Fonts } from '../../utils/Constants';

export const styles = StyleSheet.create({
    containerCalendar: {
        width: calcWidth(100),
        backgroundColor: Color.backgroundWhite,
        paddingVertical: calcHeight(3)
    },
    titleDemo: {
        fontFamily: Fonts.gothamMedium,
        fontSize: calcWidth(4),
        color: Color.cyanDark,
        alignSelf: 'center'
    },
    subTitleDemo: {
        fontFamily: Fonts.gothamBook,
        fontSize: calcWidth(3.4),
        marginTop: calcHeight(1),
        color: '#21262b',
        alignSelf: 'center'
    },
    subTitleSlotDemo: {
        fontFamily: Fonts.gothamMedium,
        fontSize: calcWidth(3.4),
        marginTop: calcHeight(2),
        color: '#21262b',
        alignSelf: 'center',
        width: calcWidth(60),
        textAlign: 'center',
        lineHeight: 20,
    },
    growText: {
        marginTop: calcHeight(6),
        width: calcWidth(90),
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        //  position:"absolute",
        height: calcHeight(5),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignSelf: 'center'
    },
    scroll: {
    },

    lineView: {
        width: calcWidth(90),
        height: calcHeight(0.1),
        marginVertical: calcHeight(1),
        backgroundColor: Color.silver,
        alignSelf: 'center'
    },
    authorText: {
       // marginTop: calcHeight(1.5),
        fontFamily: Fonts.gothamMedium,
        fontSize: calcWidth(3),
        alignItems: 'center',
        textAlign: 'left',
        color: '#858585',
    },
    slotfillingText: {
        // marginTop: calcHeight(1.5),
         fontFamily: Fonts.gothamBook,
         fontSize: calcWidth(3),
         alignItems: 'center',
         textAlign: 'left',
         color: '#595c60',
         alignSelf: 'center'
     },
    button: {
        backgroundColor: Color.cyan,
        height: calcHeight(4.5),
        width: calcWidth(80),
        marginVertical: 5,
        borderRadius: 5,
        marginHorizontal: 5,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        borderColor: '#e1e1e1'
    },
    buttonText: {
        color: Color.white,
        fontSize: calcWidth(2.5),
        fontFamily: Fonts.gothamMedium,
    },
    titleText: {
        marginTop: calcHeight(0.6),
        fontFamily: Fonts.gothamMedium,
        fontSize: calcWidth(4),
        textAlign: 'left',
        color: Color.blackText,
        lineHeight: calcHeight(3)
    },
    rangeText: {
        fontFamily: Fonts.sfSemiBold,
        width: calcWidth(90),
        alignSelf: 'center',
        fontSize: calcWidth(5.5),
        color: Color.blackText,
        marginTop: 10
    },
    seeMore: {
        width: calcWidth(80),
        fontFamily: Fonts.sfMedium,
        fontSize: calcWidth(3.5),
        color: Color.darkGreyText
    },
    bottom: {
        position: 'absolute',
        bottom: calcHeight(2),
        flexDirection: 'row',
        justifyContent: 'center',
    },

    labelText: {
        color: Color.blackText,
        fontSize: calcWidth(4.8),
        fontFamily: Fonts.gothamMedium,
        width: calcWidth(90),
        margin: 10,
        marginHorizontal: calcWidth(4),
        marginTop: calcHeight(1.5)
    },
    labelSubText: {
        color: Color.darkGreyText,
        fontSize: calcWidth(3.5),
        fontFamily: Fonts.sfRegular,
        margin: 10,
        marginTop: calcHeight(0.01),
        marginHorizontal: calcWidth(4),
        lineHeight: calcHeight(2.5)
    },
    orangeStyle: {
        backgroundColor: Color.orange,
        //  height: calcHeight(3)
        alignItems: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        paddingHorizontal: calcWidth(3),
        paddingVertical: calcWidth(1),
        borderRadius: calcWidth(20),
        marginRight: calcWidth(2)

    },
    begText: { color: 'white', fontSize: calcWidth(3), textAlign: 'center' },
    begSubText: {
        color: Color.darkGreyText,
        fontSize: calcWidth(3), textAlign: 'center',
        marginLeft: 10
    },
    flatCourse: {
        backgroundColor: Color.lightWhiteGrey,
        width: calcWidth(90),
        // height: calcHeight(7),
        marginVertical: 5,
        alignSelf: 'center',
        paddingBottom: 10
    },
    dayFlatlist: {
        backgroundColor: Color.white,
        borderRadius: 4,
        margin: 6,
        paddingHorizontal: 6,
        paddingVertical: 6,
        borderWidth: 1,
        borderColor: '#e1e1e1',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    },
    bottomTab: {
        backgroundColor: Platform.OS === 'ios' ? 'white' : 'transparent',
        width: calcWidth(100),
        justifyContent: 'space-between',
        paddingHorizontal: calcWidth(5),
        alignItems: 'center',
        shadowRadius: 1,
        shadowOpacity: 0.1,
        shadowOffset: {
            width: 0,
            height: -3,
        },
        shadowColor: '#000000',
        elevation: 2,
        borderTopColor: 'transparent',
        borderTopWidth: 1,
        flexDirection: 'row',
        alignContent: 'center',
        height: Platform.OS === 'ios' ? calcHeight(8) : calcHeight(9.5),
        marginBottom: Platform.OS === 'ios' ? 1 : -1,
    },
    bottomLabelText: {
        color: Color.blackText,
        fontSize: calcWidth(4),
        fontFamily: Fonts.gothamMedium,
    }
})