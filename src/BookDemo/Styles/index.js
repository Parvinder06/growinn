import { StyleSheet } from 'react-native';
import { calcHeight, calcWidth, calcHeightUsingPixel } from '../../utils/Dimensions'
import { Color } from '../../utils/Color';
import { Fonts } from '../../utils/Constants';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: Color.backgroundWhite
  },
  containerDemo: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white'
  },
  containerAge: {
    backgroundColor: Color.lightWhiteOrange,
    borderRadius: 5,
    padding: 10,
    marginTop: calcHeight(1.8),
    width: calcWidth(90),
    height: calcHeight(7.1)
  },
  ageTitle: {
    color: Color.orange,
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(5),
  },
  ageSubTitle: {
    color: Color.charcoalBlack,
    fontFamily: Fonts.gothamBook,
    marginTop: calcHeight(0.5),
    fontSize: calcWidth(2.8),
  },
  scroll: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    width: calcWidth(90),
    height: calcHeight(22),

  },
  scrollParent: {
    width: calcWidth(90),
    height: calcHeight(15),

  },
  scrollView: {
    borderRadius: calcWidth(5),
    height: calcHeight(4),
    borderWidth: 1,
    borderColor: '#707070',
    paddingHorizontal: calcWidth(4),
    justifyContent: 'center',
    marginVertical: calcHeight(1),
    marginHorizontal: calcWidth(1)
  },
  ageItem: {
    color: '#333333',
    fontFamily: Fonts.gothamBook,
    fontSize: calcWidth(2.8),
  },
  bgOrange: {
    position: 'absolute', bottom: calcHeight(15),
    height: calcHeight(5), width: calcWidth(90),
    backgroundColor: Color.lightWhiteOrange,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  catTitle: {
    color: Color.charcoal,
    fontFamily: Fonts.gothamBold,
    fontSize: calcWidth(4),
    position: 'absolute',
    bottom: 10,
    left: 10
  },
  bgCat: {
    width: calcWidth(42.5),
    height: calcHeight(14),
    margin: 10,
    marginVertical: calcHeight(1.2),
    borderRadius: 10,
    shadowOpacity: 0.1,
    shadowOffset: {
      width: 5,
      height: 5,
    },
    shadowColor: '#000000',
    elevation: 4,
  },
  bgInnerCat: {
    width: calcWidth(42.5),
    height: calcHeight(14),
    borderRadius: 10,
  },
  titleCat: {
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(3.5),
    color: Color.charcoal
  },
  catParentText: {
    marginLeft: calcWidth(5),
    marginTop: calcHeight(3),
    marginBottom: calcHeight(1.5),
    width: calcWidth(90)
  },
  suggestedParent: {
    marginLeft: calcWidth(6),
    // height: calcHeight(),
    width: calcWidth(60),
  },
  courseDetail: {
    // height: calcHeight(),
    width: calcWidth(60),
    marginTop: calcHeight(2)
  },
  flatCourseItem: {
    height: calcHeight(16),
    width: calcWidth(60),
    borderRadius: calcWidth(4),
    marginBottom: calcHeight(1)
  },
  courseText: {
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(3),
    color: Color.charcoal
  },
  courseTextCut: {
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(3),
    color: Color.charcoal,
    textDecorationLine: 'line-through'
  },
  priceText: {
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(3),
    color: Color.orange,
    marginTop: calcHeight(1.5)
  },
  priceTextlarge: {
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(3.5),
    color: Color.orange,
    marginTop: calcHeight(1.5)
  },
  courseSubText: {
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(2),
    color: Color.charcoal,
    marginTop: calcHeight(1)
  },
  titleCourse: {
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(4),
    color: Color.cyanDark
  },
  bgOutput: {
    backgroundColor: Color.lightSky,
    width: calcWidth(85),
    borderRadius: 10,
    padding: calcWidth(3),
    paddingVertical: calcHeight(2)
  },
  outTitle: {
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(4),
    color: Color.cyanDark
  },
  outSubTitle: {
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(2.5),
    color: Color.textLightBlack,
    lineHeight: 20,
    marginTop: calcHeight(1)
  },
  bgRequirement: {
    backgroundColor: Color.lightBlueShade,
    width: calcWidth(85),
    borderRadius: 10,
    padding: calcWidth(3),
    paddingVertical: calcHeight(2),
    borderWidth: 1,
    borderColor: '#d9dde2',
    marginTop: calcHeight(3),
    marginBottom: calcHeight(10)
  },
  reqTitle: {
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(4),
    color: Color.darkBlueText,
    marginBottom: 10
  },
  containerCalendar: {
    width: calcWidth(100),
    backgroundColor: Color.backgroundWhite,
    paddingVertical: calcHeight(3)
  },
  titleDemo: {
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(4),
    color: Color.cyanDark,
    alignSelf: 'center'
  },
  subTitleDemo: {
    fontFamily: Fonts.gothamBook,
    fontSize: calcWidth(3.4),
    marginTop: calcHeight(1),
    color: '#21262b',
    alignSelf: 'center'
  },
  subTitleSlotDemo: {
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(3.4),
    marginTop: calcHeight(1),
    color: '#21262b',
    alignSelf: 'center',
    width: calcWidth(60),
    textAlign: 'center',
    lineHeight: 20,
  },
  reqView: {
    flexDirection: 'row',
    width: calcWidth(25),
    marginHorizontal: calcWidth(1),
    alignItems: 'flex-start',
  },
  dot: {
    width: 6,
    height: 6,
    borderRadius: 3,
    backgroundColor: Color.orange,
    marginTop: 8,
    marginRight: 5
  },
  outReqSubTitle: {
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(2.5),
    color: Color.textLightBlack,
    lineHeight: 20,
  },
  floatingButton: {
    flexDirection: 'row',
    backgroundColor: Color.lightGreen,
    padding: 10,
    width: calcWidth(90),
    position: 'absolute',
    bottom: calcHeight(5),
    borderRadius: 5,
    justifyContent: 'space-between',
    alignItems: 'center',
    shadowOffset: { width: 5, height: 5 },
    shadowColor: Color.lightGreen,
    shadowOpacity: 0.2
  },
  addToCartText: {
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(2.5),
    color: 'white',
  },
  addToCartSubText: {
    fontFamily: Fonts.gothamBook,
    marginTop: 5,
    fontSize: calcWidth(2.5),
    color: 'white',
  }
})