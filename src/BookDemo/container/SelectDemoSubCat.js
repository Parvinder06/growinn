import React, { Component, useState, useEffect } from 'react';
import { View, Image, FlatList, TouchableOpacity, Text, StatusBar, BackHandler, Linking, Platform } from 'react-native';
import { calcHeight, calcWidth } from '../../utils/Dimensions'
import { Actions } from 'react-native-router-flux';
import { styles } from './../Styles';
import { Color } from '../../utils/Color';
import DataManager from '../../../src/database/DataManager';
import { connect } from 'react-redux';
import LineButton from '../../components/Button/LineButton';
import FastImage from 'react-native-fast-image';
import { BackHeader } from '../../components/BackHeader';
import AgeScreenIc from '../../assets/images/ageScreen.svgx';
import AgeTopView from '../component/AgeTopView';
import { ScrollView } from 'react-native';
import { GET_AGE } from '../../apollo/query/Demo'
import { useQuery } from '@apollo/client';

//const data = [{ test: 'Dance' }, { test: 'Mind Gym' }, { test: 'Music' }, { test: 'Coding' }, { test: 'Fitness' }, { test: 'Art Form' }]

export const SelectDemoSubCat = (props) => {
    const [selectedId, setSelectedId] = useState(null);
    const [selectedStart, setSelectedStart] = useState(null);
    const [selectedEnd, setSelectedEnd] = useState(null);
    const [data , setData] = useState(props.data.subCategories)
    // const {loading, error, data}  = useQuery(GET_AGE)

    const selectItem = (item) => {
        setSelectedId(item.test)
        //setSelectedStart(item.startAge)
        //setSelectedEnd(item.endAge)
    }

    const renderButtonDemo = (item) => {
        return (
            <LineButton
                buttonText={'Select'}
                width={calcWidth(25)}
                height={calcHeight(3.5)}
                marginTop ={calcHeight(1)}
                pressed={() => {
                    nextScreen(item)
                }}
            />
        )
    }

    const renderCatItem = (item, index) => {
        return (
            <View style={[styles.suggestedParent]}>
                <FastImage style={styles.flatCourseItem}
                    source={{
                        uri: item.webImage,
                        priority: FastImage.priority.high,
                    }}>
                </FastImage>
                <Text style={styles.courseText}>{item.name}</Text>
                <Text style={styles.courseSubText}>{item.tagLine}</Text>
                <Text style={styles.priceText}>{'₹ '}{item.demoPrice}</Text>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: calcHeight(4)}}>
                    {renderButtonDemo(item)}
                </View>
            </View>
        );
    }

    const nextScreen = (item) => {
        const id = props?.navigation?.state?.params?.id
        const start = props?.navigation?.state?.params?.startAge
        const end = props?.navigation?.state?.params?.endAge
        Actions.demo({
            id: id,
            startAge: start,
            endAge: end,
            data: item
        })
    }


    const renderList = () => {
        const numColumns = Math.ceil(data.length / 2);
        return <ScrollView
            horizontal
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={{ paddingVertical: 1 }}>
            <FlatList
                scrollEnabled={false}
                numColumns={numColumns}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                data={data}
                renderItem={({ item, index }) => renderCatItem(item, index)} />
        </ScrollView>
    }

    const start = props?.navigation?.state?.params?.startAge
    const end = props?.navigation?.state?.params?.endAge
    const catId = props?.navigation?.state?.params?.data


    return (
        <View style={styles.container}>
            <BackHeader props={props} />
            <AgeTopView title={`Your Age (${start}-${end} Years)`} subTitle={`selected category: ${catId.test}`} />
            <View onPress={() => { }}
                style={styles.catParentText}>
                <Text style={styles.titleCat}>{`Suggested for you`}</Text>
            </View>
                {renderList()}
        </View>
    );

};

export default SelectDemoSubCat;
