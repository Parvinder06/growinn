import React, { Component, useState, useEffect } from 'react';
import { View, Image, FlatList, TouchableOpacity, Text, StatusBar, BackHandler, Linking, Platform } from 'react-native';
import { calcHeight, calcWidth } from '../../utils/Dimensions'
import { Actions } from 'react-native-router-flux';
import { styles } from './../Styles';
import { Color } from '../../utils/Color';
import DataManager from '../../../src/database/DataManager';
import { connect } from 'react-redux';
import LineButton from '../../components/Button/LineButton';
import FastImage from 'react-native-fast-image';
import { BackHeader } from '../../components/BackHeader';
import AgeScreenIc from '../../assets/images/ageScreen.svgx';
import AgeTopView from '../component/AgeTopView';
import { ScrollView } from 'react-native';
import {GET_AGE} from '../../apollo/query/Demo'
import { useQuery } from '@apollo/client';

export const AgeScreen = (props) => {
    const [selectedId, setSelectedId] = useState(null);
    const [selectedStart, setSelectedStart] = useState(null);
    const [selectedEnd, setSelectedEnd] = useState(null);
    const {loading, error, data}  = useQuery(GET_AGE)

    const selectItem =(item) =>{
        setSelectedId(item.id)
        setSelectedStart(item.startAge)
        setSelectedEnd(item.endAge)
    }

    const nextScreen = () =>{
        Actions.selectDemoCategory({
            id : selectedId,
            startAge : selectedStart,
            endAge : selectedEnd
        })
    }

    const renderAgeGroup = () => {
        return <View style={styles.scrollParent}>
            <ScrollView style={styles.scrollParent}>
                <View style={styles.scroll}>
                    {data.ageGroups.map(item => {
                        return (
                            <TouchableOpacity style={styles.scrollView} onPress = {() => selectItem(item)}>
                                <Text style={styles.ageItem}>{item.startAge} - {item.endAge} Years</Text>
                            </TouchableOpacity>
                        )
                    })}
                </View>
            </ScrollView>
        </View>

    }

    const renderButton = () => {
        return (
            <View style={{ position: 'absolute', bottom: calcHeight(9) }}>
                <LineButton
                    buttonText={'Next'}
                    width={calcWidth(90)}
                    height={calcHeight(4.78)}
                    pressed={() => nextScreen()}
                />
            </View>
        )
    }

    const renderSelected = () => {
        return (
            <View style={styles.bgOrange}>
                <Text style={styles.ageItem}>{selectedStart} - {selectedEnd} Years</Text>
            </View>
        )
    }

    return (
        <View style={styles.container}>
            <BackHeader props={props} />
            <AgeTopView title={'Book a Demo'} subTitle={`Select your child's age`} />
            <View style={{ marginVertical: calcHeight(2) }}>
                <AgeScreenIc />
            </View>
            {selectedId ? renderSelected() : null}
            {data ? renderAgeGroup() : null}
            {selectedId ?renderButton() : null}
        </View>
    );

};

export default AgeScreen;
