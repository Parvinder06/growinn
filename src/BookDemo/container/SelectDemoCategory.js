import React, { Component, useState, useEffect } from 'react';
import { View, Image, FlatList, TouchableOpacity, Text, StatusBar, BackHandler, Linking, Platform } from 'react-native';
import { calcHeight, calcWidth } from '../../utils/Dimensions'
import { Actions } from 'react-native-router-flux';
import { styles } from './../Styles';
import { Color } from '../../utils/Color';
import DataManager from '../../../src/database/DataManager';
import { connect } from 'react-redux';
import LineButton from '../../components/Button/LineButton';
import FastImage from 'react-native-fast-image';
import { BackHeader } from '../../components/BackHeader';
import AgeScreenIc from '../../assets/images/ageScreen.svgx';
import AgeTopView from '../component/AgeTopView';
import { ScrollView } from 'react-native';
import { GET_CATEGORIES } from '../../apollo/query/Home'
import { useQuery } from '@apollo/client';
import SvgUri from 'react-native-svg-uri'; // SVG Package

//const data = [{ test: 'Dance' }, { test: 'Mind Gym' }, { test: 'Music' }, { test: 'Coding' }, { test: 'Fitness' }, { test: 'Art Form' }]

export const SelectDemoCategory = (props) => {
    const [selectedId, setSelectedId] = useState(null);
    const [selectedStart, setSelectedStart] = useState(null);
    const [selectedEnd, setSelectedEnd] = useState(null);
    const [selectedItem, setSelectedItem] = useState(null);
    const { loading, error, data } = useQuery(GET_CATEGORIES)

    // const {loading, error, data}  = useQuery(GET_AGE)

    const selectItem = (item) => {
        setSelectedId(item.name)
        setSelectedItem(item)
        //setSelectedStart(item.startAge)
        setSelectedEnd(item.name)
    }

    const renderCatItem = (item, index) => {
        
        return (
            <TouchableOpacity style={styles.bgCat} onPress={() => selectItem(item)}>
                    <FastImage style={styles.bgInnerCat}
                    source={{
                        uri: item.webImage,
                        priority: FastImage.priority.high,
                    }}>
                </FastImage>
                <Text style={styles.catTitle}>{item.name}</Text>
            </TouchableOpacity>
        );
    }

    const nextScreen = () => {
        const id = props?.navigation?.state?.params?.id
        const start = props?.navigation?.state?.params?.startAge
        const end = props?.navigation?.state?.params?.endAge

        Actions.selectDemoSubCat({
            id: id,
            startAge: start,
            endAge: end,
            data: selectedItem
        })
    }


    const renderList = () => {
        return <FlatList
            numColumns={2}
            data={data?.categories}
            style={{ marginTop: calcHeight(2)}}
            renderItem={({ item, index }) => renderCatItem(item, index)} />
    }

    const renderButton = () => {
        return (
            <View style={{ position: 'absolute', bottom: calcHeight(9) }}>
                <LineButton
                    buttonText={'Continue'}
                    width={calcWidth(90)}
                    height={calcHeight(4.78)}
                    pressed={() => {
                        nextScreen()
                    }}
                />
            </View>
        )
    }

    const renderSelected = () => {
        return (
            <View style={styles.bgOrange}>
                <Text style={styles.ageItem}>Selected : {selectedEnd}</Text>
            </View>
        )
    }

    const start = props?.navigation?.state?.params?.startAge
    const end = props?.navigation?.state?.params?.endAge

    return (
        <View style={styles.container}>
            <BackHeader props={props} />
            <AgeTopView title={`Your Age (${start}-${end} Years)`} subTitle={`Now Choose category`} />
            <View style={{ marginVertical: calcHeight(1), height: calcHeight(60)  }}>
                {renderList()}
            </View>
            {selectedId ? renderSelected() : null}
            {selectedId ? renderButton() : null}
        </View>
    );

};

export default SelectDemoCategory;
