import React, { Component, useState, useEffect } from 'react';
import { View, Image, FlatList, TouchableOpacity, Text, StatusBar, BackHandler, Linking, Platform } from 'react-native';
import { calcHeight, calcWidth } from '../../utils/Dimensions'
import { Actions } from 'react-native-router-flux';
import { styles } from './../Styles';
import { Color } from '../../utils/Color';
import DataManager from '../../../src/database/DataManager';
import { connect } from 'react-redux';
import LineButton from '../../components/Button/LineButton';
import FastImage from 'react-native-fast-image';
import { BackHeader } from '../../components/BackHeader';
import NextIc from '../../assets/images/forwardWhite.svgx';
import AgeTopView from '../component/AgeTopView';
import { ScrollView } from 'react-native';
import { ADD_DEMO_TO_CART } from '../../apollo/mutation/Demo'
import { useMutation } from '@apollo/client';
import Calendar from '../component/Calendar';
import { useQuery } from '@apollo/client';
import { GET_TIME_SLOTS } from '../../apollo/query/Demo'
import moment from 'moment';

//const data = [{ test: 'Dance' }, { test: 'Mind Gym' }, { test: 'Music' }, { test: 'Coding' }, { test: 'Fitness' }, { test: 'Art Form' }]

export const Demo = (props) => {
    const [selectedId, setSelectedId] = useState(null);
    const [dateSelected, setDateSelected] = useState(null);
    const [isSelected, setSelected] = useState(null);

    const [timeSelected, setTimeSelected] = useState(null);
    const [demoData, setData] = useState(props.data)
    const [isLoading, setIsLoading] = useState(false);

    const { loading, data } = useQuery(GET_TIME_SLOTS)
    const [addToCart, { error }] = useMutation(ADD_DEMO_TO_CART, {
        onError: (err) => {
            alert(JSON.stringify(err))
            setIsLoading(false)
        },
        onCompleted: (data) => {
            console.log(JSON.stringify(data))
            alert('Success')
            setIsLoading(false)
            //  Actions.otpScreen({ phoneNumber: value, id: data?.sendOtpForRegistration?.id , data : data?.sendOtpForRegistration })
        }
    });

    const [reqData, setReqData] = useState([
        { title: 'Strong Interne Connection' },
        { title: 'Laptop' },
        { title: 'Space to move freely' },
        { title: 'Comfortable Sneakers' },
        { title: 'Comfortable Clothing' }
    ]);
    // const {loading, error, data}  = useQuery(GET_AGE)

    const selectItem = (item) => {
        setSelectedId(item.test)
        //setSelectedStart(item.startAge)
        //setSelectedEnd(item.endAge)
    }

    const addToCartCall = () => {
        setIsLoading(true)
        addToCart({
            variables:
            {
                type: "DEMO",
                cId: '',
                cName: 'Dance',
                sId: demoData.id,
                slots: timeSelected
            }
        })
    };

    const onPressSlots = (slots, date) => {
        if (slots.length > 1) { setSelected(true) }
        else {setSelected(false)}

        setTimeSelected(slots)
        setDateSelected(''+moment(date).format('dddd DD MMM'))
    }

    const renderButtonCart = () => {
        let timeSlotToShow = timeSelected.length > 0 ? timeSelected[0].startTime + ' - ' + timeSelected[0].endTime :'';
        let timeSlotToShowEnd = timeSelected.length > 1 ? timeSelected[1].startTime + ' - ' + timeSelected[1].endTime :'';

        return (
            <TouchableOpacity style={styles.floatingButton} onPress = {() => addToCartCall()}>
                <View>
                    <Text style={styles.addToCartText}>{dateSelected}</Text>
                    <Text style={styles.addToCartSubText}>{timeSlotToShow}</Text>
                    <Text style={styles.addToCartSubText}>{timeSlotToShowEnd}</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={styles.addToCartText}>{'Add to Cart '}</Text>
                    <NextIc width={15} />
                </View>
            </TouchableOpacity>
        )
    }

    const renderOutput = () => {
        return (
            <View style={styles.bgOutput}>
                <Text style={styles.outTitle}>{`Output Expected`}</Text>
                <Text style={styles.outSubTitle}>{`Lorem ipsum dolor sit amet, dictas intellegebat concludaturque ei pro, lorem sonet in cum. Ea debet.`}</Text>
            </View>
        )
    }

    const renderItem = (item) => {
        return (
            <View style={styles.reqView}>
                <View style={styles.dot} />
                <Text style={styles.outReqSubTitle}>{item.title}</Text>
            </View>
        )
    }


    const renderRequirement = () => {
        return (
            <View style={styles.bgRequirement}>
                <Text style={styles.reqTitle}>{`Requirements`}</Text>
                <FlatList
                    data={reqData}
                    numColumns={3}
                    renderItem={({ item, index }) => renderItem(item, index)}
                />
            </View>
        )
    }

    const renderCatItem = (data, index) => {
        return (
            <View style={[styles.courseDetail]}>
                <FastImage style={styles.flatCourseItem}
                    source={{
                        uri: data.webImage ? data.webImage : data.thumbnail,
                        priority: FastImage.priority.high,
                    }}>
                </FastImage>
                <Text style={styles.courseText}>{data.name}</Text>
                <Text style={styles.courseSubText}>{data.tagLine}</Text>
                <Text style={styles.priceTextlarge}>{'₹'}{data?.demoPrice}</Text>

                <View style={{ flexDirection: 'row', marginBottom: calcHeight(4), marginTop: calcHeight(1), alignItems: 'center' }}>
                    <Text style={[styles.courseText, { color: Color.lightGreen }]}>{'10%off'}{'  '}</Text>
                    <Text style={[styles.courseTextCut]}>{'₹2500 - ₹2500'}</Text>

                </View>
            </View>
        );
    }

    const renderButton = () => {
        return (
            <View style={{ position: 'absolute', bottom: calcHeight(8) }}>
                <LineButton
                    buttonText={'Continue'}
                    width={calcWidth(90)}
                    height={calcHeight(4.5)}
                    pressed={() => {
                    }}
                />
            </View>
        )
    }

    const start = props?.navigation?.state?.params?.startAge
    const end = props?.navigation?.state?.params?.endAge
    const catId = props?.navigation?.state?.params?.data

    return (
        <View style={styles.containerDemo}>
            <BackHeader props={props} />
            <ScrollView>
                <Calendar onPressSlots={(slots, date) => onPressSlots(slots, date)} timeSlots={data} />
                <View style={styles.catParentText}>
                    <Text style={styles.titleCourse}>{`Course Detail`}</Text>
                    {renderCatItem(demoData)}
                    {renderOutput()}
                    {renderRequirement()}
                </View>
            </ScrollView>
            {isSelected ? renderButtonCart() : null}

        </View>
    );

};

export default Demo;
