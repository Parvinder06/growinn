import React, { useState, useEffect } from 'react';
import { View, Image, Text, TouchableOpacity, TextInput, KeyboardAvoidingView, Keyboard, StatusBar, BackHandler, TouchableWithoutFeedback, ImageBackground } from 'react-native';
import { calcHeight, calcWidth } from '../../utils/Dimensions'
import { Actions } from 'react-native-router-flux';
import { styles } from '../Styles';
import { Color } from '../../utils/Color';
import DataManager from '../../database/DataManager';
import { Platform } from 'react-native';
import { SETUP } from '../../assets/images/index';
import EmailIc from '../../assets/images/emailinput.svgx';
import UserIc from '../../assets/images/user.svgx';
import { CirclesLoader } from 'react-native-indicator';
import LinearGradient from 'react-native-linear-gradient';
import { useMutation } from '@apollo/client';;
import { CREATE_USER } from '../../apollo/mutation/login'
import apolloClient from '../../apollo';
const SetUpScreen = (props) => {
    const [userText, setUserText] = useState('');
    const [emailText, setEmailText] = useState('');

    const [isDisabled, setIsDisabled] = useState(true);
    const [value, setValue] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [otpData, setOtpData] = useState(null);
    const [otpError, setOtpError] = useState(null);
    const [createUser, { loading, error }] = useMutation(CREATE_USER, {
        onError: (err) => {
            console.log(JSON.stringify(err))
            setIsLoading(false)
            alert(JSON.stringify(err))
        },
        onCompleted: (data) => {
            console.log(JSON.stringify(data))
            setIsLoading(false)
            nextScreen(data)
        }
    });

    useEffect(() => {
        DataManager.setInitialScreen('setup');
        const { navigation } = props;
        /* this.focusListener = navigation.addListener('didFocus', () => {
            DataManager.setInitialScreen('setup');
        }); */
        BackHandler.addEventListener('hardwareBackPress', handleBackButton());
    });

    const handleBackButton = () => {
    }

    setClient = async () =>{
        let token = await AsyncStorage.getItem('Token_Key','')
        return apolloClient(token)
        }
      

    const nextScreen = async (data) => {
       await DataManager.setToken(data?.createUser?.token)
       await DataManager.setProfileData(data?.createUser?.user)
       this.setClient()
        Actions.rootTab()
    }

    const onChangeUserText = (text) => {
        setUserText(text)
        if (text.length > 0 && emailText.length > 0) {
            setIsDisabled(false)
        }
        else {
            setIsDisabled(true)
        }
    }
    const onChangeEmailText = (text) => {
        setEmailText(text)
        if (userText.length > 0 && text.length > 0) {
            setIsDisabled(false)
        }
        else {
            setIsDisabled(true)
        }
    }
    const onContinue = async () => {
        setIsLoading(true)
        createUser({
            variables:
            {
                name: userText,
                countryCode: "91",
                phoneNumber: value,
                email: emailText,
                role: 'SUPER_USER'
            }
        })
    }

    return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>

            <KeyboardAvoidingView
                behavior='position'
                style={styles.container}>
                <View >
                    {Platform.OS === 'android' ? <StatusBar
                        barStyle="dark-content"
                        hidden={true}
                        backgroundColor={Color.white}
                        translucent={false}
                        networkActivityIndicatorVisible={true}
                    /> : null}
                    <Image source={SETUP} resizeMode='cover' style={styles.bg} />
                    <LinearGradient colors={['transparent', 'transparent', 'transparent', '#000', '#000']} style={styles.bgLinear}>
                    </LinearGradient>
                    <View style={{ marginTop: calcHeight(62) }}>
                        <Text style={[styles.subTitle]}>Enter your name</Text>
                        <View style={[styles.inputTextView, { marginTop: 0,/* Platformcheck */ marginBottom: calcHeight(4) }]}>
                            <UserIc height={25} width={25} />
                            <TextInput style={styles.inputText}
                                value={userText}
                                onChangeText={value => onChangeUserText(value)}
                            />
                        </View>

                        <Text style={[styles.subTitle]}>Enter your email</Text>
                        <View style={[styles.inputTextView, { marginTop: 0 /* Platformcheck */}]}>
                            <EmailIc height={25} width={25} />
                            <TextInput style={styles.inputText}
                                value={emailText}
                                onChangeText={value => onChangeEmailText(value)}
                            />
                        </View>
                        <TouchableOpacity
                            style={[styles.LoginView, { backgroundColor: isDisabled ? Color.darkGrey : Color.lightGreen }]}
                            onPress={() => { isDisabled ? null : onContinue() }}
                            activeOpacity={isDisabled ? 1 : null}>
                            <Text style={styles.LoginButtnText}>{`Continue`}</Text>
                        </TouchableOpacity>

                    </View>
                    {isLoading ?
                        <View style={{
                            width: calcWidth(85),
                            height: calcHeight(100),
                            position: 'absolute',
                            justifyContent: 'center',
                            alignItems: 'center',
                            alignContent: 'center',
                        }}>
                            <CirclesLoader color={Color.appColor} />
                        </View> : null}
                </View>
            </KeyboardAvoidingView>

        </TouchableWithoutFeedback>
    );

};


export default (SetUpScreen);