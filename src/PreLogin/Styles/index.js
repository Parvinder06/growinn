import { StyleSheet } from 'react-native';
import { calcHeight, calcWidth } from '../../utils/Dimensions'
import { Color } from '../../utils/Color';
import { Fonts } from '../../utils/Constants';
import flavor from '../../utils/Flavor'

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: Color.lightGreenWhite,
    height: calcHeight(100),
    width: calcWidth(100)
  },
  LoginButtnText: {
    textAlign: 'center',
    paddingHorizontal: 15,
    color: Color.white,
    fontSize: calcWidth(3.2),
    fontFamily: Fonts.gothamMedium,
    width: calcWidth(70),
  },
  forgotText: {
    textAlign: 'center',
    color: Color.appColor,
    fontSize: calcWidth(3.2),
    fontFamily: Fonts.gothamMedium,
    height: calcHeight(3),
  },
  bottom: {
    position: 'absolute',
    bottom: calcHeight(2),
    flexDirection: 'row',
    justifyContent: 'center',
  },
  LoginButtons: {
    backgroundColor: Color.lightGreen,
    height: calcHeight(5),
    justifyContent: 'center',
    alignItems: 'center',
    //marginTop: calcHeight(4),
    width: calcWidth(85),
    borderRadius: calcWidth(1),
    flexDirection: 'row',
  },
  title: {
    textAlign: 'center',
    color: Color.blackText,
    fontSize: calcWidth(6),
    // //fontFamily: Fonts.sfSemiBold,
    width: calcWidth(80)
  },
  pageTitle: {
    textAlign: 'center',
    color: Color.charcoal,
    fontSize: calcWidth(5.2),
    fontFamily: Fonts.gothamMedium,
    width: calcWidth(70),
    marginTop: calcHeight(2),
    lineHeight: calcHeight(3.5)
  },
  pageSubTitle : {
    textAlign: 'center',
    color: Color.charcoal,
    fontSize: calcWidth(2.5),
    fontFamily: Fonts.gothamBook,
    marginTop: calcHeight(0.5),
    width: calcWidth(70),
    lineHeight: calcHeight(2)
  },
  viewPagerView: {
    width: calcWidth(100),
    height: calcHeight(88),
    alignContent: 'center',
    alignItems: 'center',
    // marginTop: calcHeight(5),
  },
  bannerImage: { height: calcHeight(60), width: calcWidth(100) },
  inputTextView: {
    backgroundColor: Color.transparent,
    marginVertical: calcHeight(1), // Platformcheck
    width: calcWidth(85),
    borderColor: Color.silver,
    height: calcHeight(5.5),
    borderRadius: 10,
    borderWidth: 0.5,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent:'center'
  },
  inputText: {
    width: calcWidth(70),
    opacity: 0.8,
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(3.8),
    //alignSelf: 'flex-start',
    height: calcHeight(6),
    marginLeft: 10,
    color: Color.white,
  },
  subTitle: {
    width: calcWidth(80),
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(3.5),
    alignSelf: 'flex-start',
    height: calcHeight(3),
    color: Color.white,
    marginLeft: 5,
  },
  subtitleCode: {
    fontSize: calcWidth(3.5),
    color: Color.white,
    opacity: 0.45,
    fontFamily: Fonts.gothamMedium
  },
  codeExpireText: {
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(3.2),
    height: calcHeight(3),
    color: Color.white,
  },
  LoginView: {
    backgroundColor: Color.lightGreen,
    height: calcHeight(5.5),
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: calcHeight(3),
    marginVertical: calcHeight(1.5),
    width: calcWidth(85),
    borderRadius: calcWidth(1),
    flexDirection: 'row',
  },
  LoginText: {
    textAlign: 'center',
    paddingHorizontal: 15,
    color: Color.white,
    fontSize: calcWidth(3.7),
    fontFamily: Fonts.gothamMedium,
    width: calcWidth(70),
  },
  bottom: {
    position: 'absolute',
    bottom: calcHeight(2),
    flexDirection: 'row',
    justifyContent: 'center',
  },
  continueText: {
    textAlign: 'center',
    color: Color.darkGreyText,
    fontSize: calcWidth(3.5),
    //fontFamily: Fonts.sfRegular,
    width: calcWidth(70),
    margin: calcHeight(3)
  },
  labelText: {
    textAlign: 'left',
    color: Color.blackText,
    fontSize: calcWidth(3.5),
    //fontFamily: Fonts.sfMedium,
    width: calcWidth(90),
    marginTop: calcHeight(1.5)
  },
  labelText2: {
    textAlign: 'left',
    color: Color.darkGreyText,
    fontSize: calcWidth(3.5),
    //fontFamily: Fonts.sfRegular,
    width: calcWidth(90),
    marginTop: calcHeight(1.5)
  },
  underlineStyleBase: {
    width: calcWidth(12),
    height: flavor.isAndroid ? calcWidth(12) : calcWidth(12),
    borderWidth: 1,
    borderBottomWidth: 1,
    borderRadius: 8,
    borderColor: '#d5d7e2',
    fontFamily: Fonts.gothamMedium,
    color: Color.white,
    fontSize: calcWidth(4.5),
    backgroundColor: 'transparent',
  },

  underlineStyleHighLighted: {
    fontFamily: Fonts.gothamMedium,
  },
  phoneView: {
    width: calcWidth(90),
    height: calcHeight(6),
    flexDirection: 'row',
    marginBottom: calcHeight(2.5),
    alignSelf: 'center',
    backgroundColor: Color.backgroundText,
    borderRadius: calcWidth(1),
    borderColor: Color.silver,
    borderWidth: 1,
    alignItems: 'center',
    paddingHorizontal: 10
  },
  ageItem: {
    overflow: 'visible',
    borderColor: Color.cyan,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 10,
  },
  mainPersonalize: {
    height: calcHeight(78),
    alignItems: 'center',
    marginTop: calcHeight(3),
    borderRadius: 5,
    width: calcWidth(90),
    backgroundColor: Color.cyanLight
  },
  selectAgeText: {
    //fontFamily: Fonts.sfSemiBold,
    fontSize: calcWidth(3.8),
    color: Color.darkGreyText,
    marginTop: calcHeight(5),
    textAlign: 'center'
  },
  ageSubText: {
    //fontFamily: Fonts.sfMedium,
    fontSize: calcWidth(3),
    marginTop: 10,
    width: calcWidth(60),
    lineHeight: 20,
    color: Color.darkGrey,
    textAlign: 'center'
  },
  absolute: {
    width: calcWidth(70),
    alignSelf: 'center',
    justifyContent: 'center',
    alignContent: 'center',
    marginTop: flavor.isAndroid ? calcHeight(44.5) : calcHeight(41.8),
    height: flavor.isAndroid ? calcHeight(5) : calcHeight(4),
    justifyContent: 'center',
    paddingHorizontal: 10,
    borderRadius: 20,
    paddingVertical: 1,
    borderColor: Color.lightGrey,
    borderWidth: 1,
    position: 'absolute',
  },
  flatlistParent: {
    width: calcWidth(70),
    alignSelf: 'center',
    justifyContent: 'center',
    alignContent: 'center',
    marginTop: calcHeight(5),
    justifyContent: 'center',
    paddingHorizontal: 10,
    // height: calcHeight(10)
  },
  selectedCircle: {
    height: 60,
    width: 60,
    borderRadius: 30,
    backgroundColor: Color.cyanLight,
    borderColor: Color.cyan,
    borderWidth: 3,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 10,
    alignSelf: 'center'
  },
  outline: {
    width: calcWidth(70),
    alignSelf: 'center',
    justifyContent: 'center',
    alignContent: 'center',
    height: flavor.isAndroid ? calcHeight(5) : calcHeight(5),
    justifyContent: 'center',
    borderRadius: 20,
    // paddingVertical: 1,
    borderColor: Color.lightGrey,
    borderWidth: 1,
    position: 'absolute',
    alignSelf: 'center',
    // backgroundColor:'red',
    top: flavor.isAndroid ? '50%' : '50%',
    // bottom: calcHeight(7),
  },
  backImage: {
    marginTop: flavor.isiOS ? calcHeight(7) : 5,
    alignSelf: 'flex-start',
    //marginLeft: -calcWidth(5)
  },
  bg: {
    position: 'absolute',
    height: calcHeight(100),
    //width: calcWidth(100),
    flex:1,
    marginLeft: -calcWidth(10),
    width: calcWidth(105),
  },
  TitleText: {
    textAlign: 'left',
    color: Color.white,
    fontSize: calcWidth(8.5),
    width: calcWidth(85),
    fontFamily: Fonts.gothamMedium

  },
  bgLinear: {
    position: 'absolute',
    height: calcHeight(100),
    backgroundColor: 'rgba(0,0,0,0.5)',
    marginLeft: -calcWidth(10),
    width: calcWidth(110),
  }
})