import React, { useState, useEffect } from 'react';
import { View, Image, Text, TouchableOpacity, TextInput, Keyboard, KeyboardAvoidingView, SafeAreaView, StatusBar, BackHandler, TouchableWithoutFeedback, ImageBackground } from 'react-native';
import flavor from '../../utils/Flavor';
import { calcHeight, calcWidth } from '../../utils/Dimensions'
import { Actions } from 'react-native-router-flux';
import { styles } from '../Styles';
import { Fonts, enterAddress, enterPassword } from '../../utils/Constants';
import { Color } from '../../utils/Color';
import ViewPager from '@react-native-community/viewpager';
import Banner1 from '../../assets/images/banner1.svgx';
import DataManager from '../../database/DataManager';
import { Platform } from 'react-native';
import { SPLASH1, SPLASH2, SPLASH3, LOGINMOBILE } from '../../assets/images/index';
import BackBlack from '../../assets/images/BackBlack.svgx';
import IndiaIc from '../../assets/images/india.svgx';
import { CirclesLoader } from 'react-native-indicator';
import LinearGradient from 'react-native-linear-gradient';
import { useMutation } from '@apollo/client';;
import {SEND_OTP} from '../../apollo/mutation/login'

const MobileScreen = (props) => {
    const [text, setText] = useState('');
    const [isDisabled, setIsDisabled] = useState(true);
    const [value, setValue] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [sendOtpForRegistration, { loading, error }] = useMutation(SEND_OTP, {
        onError: (err) => {
            alert(JSON.stringify(err))
            setIsLoading(false)
        },
        onCompleted: (data) => {
            console.log(JSON.stringify(data))
            setIsLoading(false)
            Actions.otpScreen({ phoneNumber: value, id: data?.sendOtpForRegistration?.id , data : data?.sendOtpForRegistration })
        }
    });

    useEffect(() => {
        DataManager.setInitialScreen('welcome');
        const { navigation } = props;
        /*  this.focusListener = navigation.addListener('didFocus', () => {
             DataManager.setInitialScreen('welcome');
         });
         BackHandler.addEventListener('hardwareBackPress', handleBackButton()); */
    });

    const handleBackButton = () => {
    }

    const send = () => {
        setIsLoading(true)
        sendOtpForRegistration({
            variables:
            {
                countryCode: "91",
                phoneNumber: value
            }
        })
    };

    const onChangeText = (text) => {
        setValue(text)
        if (text.length >= 10) {
            setIsDisabled(false)
        }
        else {
            setIsDisabled(true)
        }
    }
    const onContinue = async () => {
        send()
    }
    return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>

            <KeyboardAvoidingView
                behavior='position'
                keyboardVerticalOffset={Platform.OS === 'ios' ? -100 : -100}
                style={styles.container}>
                <View>
                    {Platform.OS === 'android' ? <StatusBar
                        barStyle="dark-content"
                        hidden={true}
                        backgroundColor={Color.white}
                        translucent={false}
                        networkActivityIndicatorVisible={true}
                    /> : null}
                    <Image source={LOGINMOBILE} resizeMode='cover' style={styles.bg} />

                    <LinearGradient colors={['transparent', 'transparent', 'transparent', '#000', '#000']} style={styles.bgLinear}>
                    </LinearGradient>

                    <View style={{ marginTop: calcHeight(73) }}>
                        <Text style={[styles.subTitle]}>Enter your mobile number</Text>
                        <View style={styles.inputTextView}>
                            <IndiaIc style={{ opacity: 0.7, marginLeft: calcWidth(8) }} height={25} width={25} />
                            <Text style={styles.subtitleCode} > +91</Text>
                            <TextInput style={styles.inputText} placeholderTextColor={Color.darkGreyText} placeholder={'Phone number'}
                                value={value}
                                keyboardType="numeric"
                                ReturnKeyType="done"
                                onChangeText={value => onChangeText(value)}
                            />
                        </View>

                        <TouchableOpacity
                            style={[styles.LoginView, { backgroundColor: isDisabled ? Color.darkGrey : Color.lightGreen }]}
                            onPress={() => { isDisabled ? null : onContinue() }}
                            activeOpacity={isDisabled ? 1 : null}>
                            <Text style={styles.LoginButtnText}>{`Get OTP`}</Text>
                        </TouchableOpacity>
                    </View>
                    {isLoading ?
                        <View style={{
                            height: calcHeight(100),
                            position: 'absolute',
                            justifyContent: 'center',
                            alignItems: 'center',
                            alignContent: 'center',
                            alignSelf:'center'
                        }}>
                            <CirclesLoader color={Color.appColor} />
                        </View> : null}
                </View>
            </KeyboardAvoidingView>
        </TouchableWithoutFeedback>
    );

};


export default (MobileScreen);