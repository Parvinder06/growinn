import React, { Component } from 'react';
import { View, Image, Alert, SafeAreaView, StatusBar, BackHandler, Linking, Platform } from 'react-native';
import { calcHeight, calcWidth } from '../utils/Dimensions'
import { Actions } from 'react-native-router-flux';
import { styles } from './Styles';
import { Color } from '../utils/Color';
import MainLogo from '../assets/images/growMainLogo.svgx';
import DataManager from '../../src/database/DataManager';
import { connect } from 'react-redux';
import {
    getUpdateConfigStatus, resetUpdateConfig
} from '../store/actions/index';

class Splash extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isUpdated: 3,
            url: null
        }
    }


    clearAll = () => {
        this.setState({
            firstName: '',
            lastName: '',
            email: '',
            password: '',
        })
    }

    componentDidMount() {
        //this.props.getUpdateConfigStatus(Platform.OS, DeviceInfo.getVersion())
        setTimeout(() => {
            this.setTimePassed();
        }, 1500);
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    handleBackButton() {
    }


    setTimePassed = () => {
        DataManager.getInitialScreen().then((res) => {
            this.checkStatus(res)
        });
    }

    checkStatus = (param) => {
        switch (param) {
            case 'welcome':
                Actions.preLogin()
                break;
            case 'setup':
                Actions.setUpScreen()
                break;
            case 'home':
                Actions.rootTab()
                break;
            default:
                Actions.preLogin()
        }

    }
    render() {
        return (
            <SafeAreaView style={{ height: calcHeight(95), backgroundColor: Color.white }}>
                <View style={[styles.container, {
                    justifyContent: 'center',
                    alignItems: 'center'
                }]}>
                    {Platform.OS === 'android' ? <StatusBar
                        barStyle="dark-content"
                        // dark-content, light-content and default
                        hidden={true}
                        //To hide statusBar
                        backgroundColor="white"
                        //Background color of statusBar only works for Android
                        translucent={false}
                        //allowing light, but not detailed shapes
                        networkActivityIndicatorVisible={true}
                    /> : null}
                    <MainLogo />
                </View>
            </SafeAreaView>
        );
    }

};


const mapStateToProps = LoginReducer => {
    return LoginReducer;
};

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(Splash);
