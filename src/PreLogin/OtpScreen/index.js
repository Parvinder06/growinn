import React, { useState, useEffect } from 'react';
import { View, Image, Text, TouchableOpacity, Keyboard, KeyboardAvoidingView, StatusBar, BackHandler, TouchableWithoutFeedback, ImageBackground } from 'react-native';
import { calcHeight, calcWidth } from '../../utils/Dimensions'
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { styles } from './../Styles';
import { Color } from '../../utils/Color';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import { CirclesLoader } from 'react-native-indicator';
import { LOGINOTP } from '../../assets/images/index';
import BackBlack from '../../assets/images/BackBlack.svgx';
import LinearGradient from 'react-native-linear-gradient';
import { useQuery,useLazyQuery } from '@apollo/client';;
import {VERIFY_OTP} from '../../apollo/query/Login'
const OtpScreen = (props) => {
    const [value, setValue] = useState('');
    const [isDisabled, setIsDisabled] = useState(true);
    const [isLoading, setIsLoading] = useState(false);
    const [timer, setTimer] = useState(30);
    const [VerifyOtp, loading] = useLazyQuery(VERIFY_OTP, {
            onError: (err) => {
                console.log(JSON.stringify(err))
                alert(JSON.stringify(err))
                setIsLoading(false)
            },
            onCompleted: (data) => {
                console.log(JSON.stringify(data))
                setIsLoading(false)
                if(data?.verifyOtp?.verified){
                Actions.setUpScreen({ data: props?.navigation?.state?.params?.data })
                }
                else {
                    alert('Incorrect OTP')
                }
            }
        });

    const verify = () => {
        let userId = props?.navigation?.state?.params?.id;
        setIsLoading(true)
        VerifyOtp({
            variables:
            {
                id: userId,
                otp: value
            }
        })
    };


    useEffect(() => {
        if (!timer) return;
        const intervalId = setInterval(() => {
          setTimer(timer - 1);
        }, 1000);
            return () => clearInterval(intervalId);
      }, [timer]);

    const onChangeText = (t) => {
        if (t.length === 4) {
            setValue(t)
        }
        else {
            setIsDisabled(true)
        }
    }

    const onCodeChange = (code) => {
        if (code.length === 4) {
            setIsDisabled(false)
        }
        else {
            setIsDisabled(true)
        }
    }

    const onContinue = () => {
        verify();
    }

    const onResend = () => {
        setTimer(30)
    }

    return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
            <KeyboardAvoidingView
                behavior='position'
                keyboardVerticalOffset={Platform.OS === 'ios' ? -100 : -100}
                style={styles.container}>
                <View>
                    {Platform.OS === 'android' ? <StatusBar
                        barStyle="dark-content"
                        hidden={true}
                        backgroundColor={Color.white}
                        translucent={false}
                        networkActivityIndicatorVisible={true}
                    /> : null}
                    <Image source={LOGINOTP} resizeMode='cover' style={styles.bg} />

                    <LinearGradient colors={['transparent', 'transparent', 'transparent', '#000', '#000']} style={styles.bgLinear}>
                    </LinearGradient>

                    <TouchableOpacity onPress={() => Actions.pop()} style={styles.backImage}>
                        <BackBlack width={20} height={20} fill='#fff' />
                    </TouchableOpacity>
                    <View style={{ marginTop: calcHeight(52), width: calcWidth(85) }}>
                        <Text style={[styles.TitleText, { height: calcHeight(5) }]}>{`OTP`}</Text>
                        <Text style={[styles.subTitle]}>We've sent the code to your phone inbox</Text>
                        <View style={{
                            width: calcWidth(85),
                            height: calcHeight(6), flexDirection: 'row',
                            marginVertical: calcHeight(1.5), //Platformcheck
                            marginBottom: calcHeight(1),
                            justifyContent: "space-between",
                            alignSelf: 'center'
                        }}>
                            <OTPInputView
                                style={{ width: calcWidth(85), alignSelf: 'center' }} 
                                pinCount={4} // 6
                                placeholderCharacter={'-'}
                                autoFocusOnLoad
                                codeInputFieldStyle={styles.underlineStyleBase}
                                codeInputHighlightStyle={styles.underlineStyleHighLighted}
                                onCodeFilled={(code => {
                                    onChangeText(code)
                                })}
                                onCodeChanged={(code) => { onCodeChange(code) }}
                            />
                        </View>
                        {
                            timer === 0 ? null :
                                <View style={{ flexDirection: 'row', marginTop: calcHeight(3), justifyContent: 'center' }}>
                                    <Text style={[styles.codeExpireText]}>Code expires in : </Text>
                                    <Text style={[styles.codeExpireText, { color: Color.lightGreen }]}>00 : {timer} </Text>
                                </View>
                        }

                        <View style={{ flexDirection: 'row', marginTop: calcHeight(2), justifyContent: 'center' }}>
                            <Text style={[styles.codeExpireText]}>Didn’t receive code? </Text>
                            <TouchableOpacity onPress={() => onResend()} >
                                <Text style={[styles.forgotText, { color: Color.lightGreen }]}>Resend Code</Text>
                            </TouchableOpacity>
                        </View>
                        <TouchableOpacity
                            style={[styles.LoginView, { backgroundColor: isDisabled ? Color.darkGrey : Color.lightGreen, marginTop: calcHeight(1) }]}
                            onPress={() => { isDisabled ? null : onContinue() }}
                            activeOpacity={isDisabled ? 1 : null}>
                            <Text style={styles.LoginText}>{`Verify`}</Text>
                        </TouchableOpacity>
                    </View>
                    {isLoading ?
                        <View style={{
                            height: calcHeight(100),
                            position: 'absolute',
                            justifyContent: 'center',
                            alignItems: 'center',
                            alignContent: 'center',
                            alignSelf: 'center'
                        }}>
                            <CirclesLoader color={Color.lightGreenWhite} />
                        </View>
                        : null}
                </View>
            </KeyboardAvoidingView>
        </TouchableWithoutFeedback>
    );
}

const mapStateToProps = LoginReducer => {
    return LoginReducer;
};

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(OtpScreen);