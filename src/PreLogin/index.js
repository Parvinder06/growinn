import React, { Component } from 'react';
import { View, Image, TouchableOpacity, Text, SafeAreaView, StatusBar, BackHandler, TouchableWithoutFeedback, ImageBackground } from 'react-native';
import flavor from '../utils/Flavor';
import { calcHeight, calcWidth } from '../utils/Dimensions'
import { Actions } from 'react-native-router-flux';
import { styles } from './Styles';
import { Fonts, enterAddress, enterPassword } from '../utils/Constants';
import { Color } from '../utils/Color';
import ViewPager from '@react-native-community/viewpager';
import Banner1 from '../assets/images/banner1.svgx';
import Banner2 from '../assets/images/banner2.svgx';
import Banner3 from '../assets/images/banner3.svgx';
import Bg from '../assets/images/bgLogin.svgx'
import DataManager from '../../src/database/DataManager';
import { Platform } from 'react-native';
import { SPLASH1, SPLASH2, SPLASH3, SPLASHBG } from '../assets/images/index'
class PreLogin extends Component {

    constructor(props) {
        super(props);
        this.state = {
            page: 0
        }
    }

    componentDidMount() {
        DataManager.setInitialScreen('welcome');
        const { navigation } = this.props;
        this.focusListener = navigation.addListener('didFocus', () => {
            DataManager.setInitialScreen('welcome');
        });
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    handleBackButton() {
    }

    createAccount = () => {
        Actions.signUp()
    }

    loginAccount = () => {
        Actions.mobileScreen()
    }

    onPageSelected = (e: PageSelectedEvent) => {
        this.setState({ page: e.nativeEvent.position });
    };
    //<Banner1 height={calcHeight(100)}/>
    MyPager = () => {
        return (
            <ViewPager style={styles.viewPagerView} initialPage={0} scrollEnabled onPageSelected={this.onPageSelected}
            >
                <View key="1">
                    <View style={{ alignItems: 'center' }}>
                        <Image source={SPLASH1} resizeMode='stretch' style={styles.bannerImage} />
                        <Text style={styles.pageTitle}>India’s 360° Education Marketplace</Text>
                        <Text style={styles.pageSubTitle}>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</Text>
                    </View>
                </View>
                <View key="2">
                    <View style={{ alignItems: 'center' }}>
                        <Image source={SPLASH2} resizeMode='stretch' style={styles.bannerImage} />
                        <Text style={styles.pageTitle}>Freedom of Choice</Text>
                        <Text style={styles.pageSubTitle}>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</Text>
                    </View>
                </View>
                <View key="3">
                    <View style={{ alignItems: 'center' }}>
                        <Image source={SPLASH3} resizeMode='stretch' style={styles.bannerImage} />
                        <Text style={styles.pageTitle}>Refer & Earn </Text>
                        <Text style={styles.pageSubTitle}>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</Text>
                    </View>
                </View>

            </ViewPager>
        );
    };

    Indicator = () => {
        return (
            <View style={{ marginTop: calcHeight(1), width: calcWidth(85), alignSelf: 'center', alignItems: 'center' }}>
                {Platform.OS === 'android' ? <StatusBar
                    barStyle="dark-content"
                    // dark-content, light-content and default
                    hidden={true}
                    //To hide statusBar
                    backgroundColor={Color.appLight}
                    //Background color of statusBar only works for Android
                    translucent={false}
                    //allowing light, but not detailed shapes
                    networkActivityIndicatorVisible={true}
                /> : null}
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ backgroundColor: this.state.page === 0 ? Color.appColor : '#d1d0e7', width: this.state.page === 0 ? calcWidth(4) : calcWidth(2), height: calcWidth(1), margin: 5 }} />
                    <View style={{ backgroundColor: this.state.page === 1 ? Color.appColor : '#d1d0e7', width: this.state.page === 1 ? calcWidth(4) : calcWidth(2), height: calcWidth(1), margin: 5 }} />
                    <View style={{ backgroundColor: this.state.page === 2 ? Color.appColor : '#d1d0e7', width: this.state.page === 2 ? calcWidth(4) : calcWidth(2), height: calcWidth(1), margin: 5 }} />
                </View>
            </View>
        )
    }

    render() {
        return (
            /*             <SafeAreaView style={{ height: calcHeight(95), backgroundColor: Color.white }}>
             */
            <View style={styles.container}>
                {this.MyPager()}
                <View style={{ position: 'absolute', top: calcHeight(80) }}>
                    {this.Indicator()}
                </View>
                <View style={{ flexDirection: 'row', width: calcWidth(85), /* marginTop: calcHeight(1), */ justifyContent: 'space-between' }}>
                    <TouchableOpacity style={[styles.LoginButtons]
                    } onPress={() => { this.loginAccount() }}
                    >
                        <Text style={styles.LoginButtnText}>{`Get Started`}</Text>
                    </TouchableOpacity>
                </View>
            </View>
            /*             </SafeAreaView >
             */
        );
    }

};


export default (PreLogin);