import {
    Dimensions, PixelRatio
} from 'react-native';

const deviceWidth = Dimensions.get('window').width
const deviceHeight = Dimensions.get('window').height

export const calcHeight = (x) => {
    return PixelRatio.roundToNearestPixel((deviceHeight * x) / 100)
}
export const calcWidth = (x) => {
    return PixelRatio.roundToNearestPixel((deviceWidth * x) / 100)
}

export const calcHeightUsingPixel =(x) => {
    return PixelRatio.roundToNearestPixel((x / deviceHeight )* 100)
}
