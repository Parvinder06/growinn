import flavor from './Flavor';
import moment from 'moment';

export const Fonts = {
    gothamBlack: "Gotham-Black",
    gothamBlackItalic: "Gotham-BlackItalic",
    gothamBold: "Gotham-Bold",
    gothamBoldItalic: "Gotham-BoldItalic",
    gothamBook: "Gotham-Book",
    gothamBookItalic: "Gotham-BookItalic",
    gothamLight:"Gotham-Light",
    gothamLightItalic: "Gotham-LightItalic",
    gothamMedium: "Gotham-Medium",
    gothamMediumItalic: "Gotham-MediumItalic",
    gothamThin:"Gotham-Thin",
    gothamThinItalic:"Gotham-ThinItalic",
    gothamUltra: "Gotham-Ultra",
    gothamUltraItalic : "Gotham-UltraItalic",
    gothamXLight:"Gotham-XLight",
    gothamXLightItalic:"Gotham-XLightItalic"
}

export const ToastMessageType = {
    INFO: "#f48220",
    SUCCESS: "green",
    ERROR: "red",
}

export const enterAddress = 'Enter email';
export const enterPassword = 'Password';

export const dates = () => {
    let days = [];
    let startdate = moment()
    days.push(moment(startdate, "DD-MM-YYYY"))
    for (let index = 1; index <= 100; index++) {
        var day = moment(startdate, "DD-MM-YYYY").add(index, 'days');
        days.push(day);
    }
    return days;
}