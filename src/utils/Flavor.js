import {
    Platform
} from 'react-native';

class Flavor {
    constructor() {
        if (this.instance) {
            return;
        }
        this.instance = this;
        this.isAndroid = (Platform.OS === "android")
        this.isiOS = (Platform.OS === "ios")
    }
    getkeys = () => {
            return ({
              //  API_URL: "https://backend.growinnsteps.com/",
              API_URL: "https://test-backend.growinnsteps.com/"
            })
        }
}

export default new Flavor();
