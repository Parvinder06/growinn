import React, { Component, useState, useEffect } from 'react';
import { View, Image, FlatList, TouchableOpacity, Text, StatusBar, BackHandler, Linking, Platform } from 'react-native';
import { calcHeight, calcWidth } from '../../utils/Dimensions'
import { Actions } from 'react-native-router-flux';
import { styles } from './../Styles';
import { Color } from '../../utils/Color';
import DataManager from '../../../src/database/DataManager';
import { connect } from 'react-redux';
import LineButton from '../../components/Button/LineButton';
import FastImage from 'react-native-fast-image';
import { BackHeader } from '../components/BackHeader';
import NextIc from '../../assets/images/forwardWhite.svgx';
import { ScrollView } from 'react-native';
import Video from 'react-native-video';
import { useMutation } from '@apollo/client';
import { useQuery } from '@apollo/client';
import { GET_TIME_SLOTS } from '../../apollo/query/Demo'
import moment from 'moment';
import IcPlay from '../../assets/images/playIc.svgx';

const url = 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4';
const data = [{
    id: 1,
    title: 'Obtain a strong understanding on the fundamentals of programming'
},
{
    id: 2,
    title: 'Obtain a strong understanding on the fundamentals of programming'
},
]

export const Course = (props) => {
    const [selectedId, setSelectedId] = useState(null);
    const [pausedValue, setPaused] = useState(false);

    const renderVideo = () => {
        return <TouchableOpacity onPress={() => setPaused(!pausedValue)}>
            <Video source={{ uri: url }}
                style={styles.videoItem}
                paused={pausedValue}
                resizeMode={'cover'}
                playInBackground={false}
                playWhenInactive={false}
                hideShutterView={true}
                // poster={this.state.thumbnailUrl}
                posterResizeMode={'cover'}
            />
        </TouchableOpacity>
    }
    const renderButtonCart = () => {
        return (
            <TouchableOpacity style={styles.floatingButton} /* onPress = {() => } */>
                <View>
                    <Text style={styles.addToCartSubText}>{'Rs. 220.40'}</Text>
                    <Text style={styles.addToCartText}>{'Rs. 220.40'}</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={styles.addToCartText}>{'Add to Cart '}</Text>
                    <NextIc width={15} />
                </View>
            </TouchableOpacity>
        )
    }

    const renderTopTitle1 = () => {
        return (
            <View style={styles.bgTop}>
                <Text style={styles.title}>{`Last Updates`}</Text>
                <Text style={styles.subtTitleBlack}>{`Oct 2021`}</Text>
            </View>
        )
    }

    const renderTopTitle3 = () => {
        return (
            <View style={styles.bgTop}>
                <Text style={styles.title}>{`Level`}</Text>
                <Text style={styles.subtTitleBlack}>{props?.data?.levels[0].levelName}</Text>
            </View>
        )
    }

    const renderTopTitle4 = () => {
        return (
            <View style={styles.bgTop}>
                <Text style={styles.title}>{`Students`}</Text>
                <Text style={styles.subtTitleBlack}>{`21,457`}</Text>
            </View>
        )
    }

    const renderTopTitle2 = () => {
        return (
            <View style={styles.bgTop}>
                <Text style={styles.title}>{`Language`}</Text>
                <Text style={styles.subtTitleBlack}>{`English`}</Text>
            </View>
        )
    }

    const renderPlay = () => {
        return (
            <IcPlay />
        )
    }

    const renderItem = (item, index) => {
        return (
            <View style={styles.itemStyle}>
                <Text style={styles.itemTick}>✔</Text>
                <Text style={styles.HeaderSubtTitleBlack}>{item.description}</Text>
            </View>
        )
    }

    const whatYouWillLearn = () => {
        return (
            <View style={styles.bgWhite}>
                <Text style={styles.headerTitle}>What you'll learn</Text>
                <FlatList
                    data={props?.data?.includes}
                    renderItem={({ item, index }) => renderItem(item, index)}
                />
            </View>
        )
    }

    const requirements = () => {
        return (
            <View style={styles.bgWhite}>
                <Text style={styles.headerTitle}>Requirements</Text>
                <FlatList
                    data={props?.data?.requirements}
                    renderItem={({ item, index }) => renderItem(item, index)}
                />
            </View>
        )
    }

    const renderOffer = () => {
        return (
            <View style={styles.row}>
                <View style={styles.orangeView}>
                    <Text style={styles.textWhite}>28% Off</Text>
                </View>
                <Text style={styles.textOrange}>Congrats! You can apply code GROWINN to get this offer.</Text>

            </View>
        )
    }

    return (
        <View style={styles.container}>
            <BackHeader props={props} title={props?.data?.name} rating={props?.data?.rating} level ={props?.data?.levels[0].levelName} />
            <ScrollView style={{ marginBottom: calcHeight(15) }}>
                <View style={styles.topView}>
                    <View style={styles.row}>
                        {renderTopTitle1()}
                        {renderTopTitle2()}
                    </View>
                    <View style={styles.row}>
                        {renderTopTitle3()}
                        {renderTopTitle4()}
                    </View>
                </View>
                <View style={{ marginTop: calcHeight(4) }}>
                    {renderVideo()}
                    {pausedValue ? <TouchableOpacity onPress={() => setPaused(!pausedValue)}
                        style={{ position: 'absolute', alignSelf: 'center', marginTop: calcHeight(7) }}>
                        {renderPlay()}
                    </TouchableOpacity> : null}
                </View>
                <View style={styles.bgWhite}>
                    <Text style={styles.headerTitle}>Overview</Text>
                    <Text style={styles.HeaderSubtTitleBlack}>{props?.data?.fullDescription}</Text>
                </View>
                {whatYouWillLearn()}
                {requirements()}
                {renderOffer()}
            </ScrollView>
            {renderButtonCart()}
        </View>
    );

};

export default Course;
