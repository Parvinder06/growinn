import React, { Component, useState, useEffect } from 'react';
import { View, Image, FlatList, TouchableOpacity, Text, StyleSheet, BackHandler, Linking, Platform } from 'react-native';
import { calcHeight, calcWidth } from '../../../utils/Dimensions'
import { Actions } from 'react-native-router-flux';
import { Color } from '../../../utils/Color';
import BackBlack from '../../../assets/images/BackBlack.svgx';
import { Fonts } from '../../../utils/Constants';
import { Rating, AirbnbRating } from 'react-native-ratings';

export const BackHeader = (props) => {
    return (
        <View style={styles.container}>
            <View style={styles.top} />
            <View style={styles.innerView}>
                <TouchableOpacity style={styles.white} onPress={() => Actions.pop()}>
                    <BackBlack width={16} height={16} />
                </TouchableOpacity>
                <View>
                    <Text style={styles.text}>{props.title}</Text>
                    <View style={styles.row}>
                        <View style={styles.orangeView}>
                            <Text style={styles.subTitleText}>{props.level}</Text>
                        </View>
                        <Text style={[styles.subTitleText, {marginRight:5}]}>{props.rating}</Text>
                        <Rating
                            ratingCount={5}
                            readonly
                            imageSize={12}
                            isDisabled ={true}
                            startingValue={props.rating}
                            showRating ={false}
                            ratingColor='#ffc82c'
                        />
                    </View>
                </View>
            </View>
        </View>
    );

};

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        width: calcWidth(100),
        height: calcHeight(12),
    },
    top: {
        backgroundColor: Color.appThemeColor,
        width: calcWidth(100),
        height: calcHeight(5)
    },
    white: {
        backgroundColor: 'white',
        width: calcWidth(10),
        height: calcHeight(7),
        justifyContent: 'center',
        paddingHorizontal: 10
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 5,
    },
    innerView: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    orangeView: {
        backgroundColor: '#fee3dc',
        padding: 6,
        paddingHorizontal: 8,
        borderRadius: 5,
        marginRight: calcWidth(3)
    },
    text: {
        color: '#696969',
        fontSize: calcWidth(3.2),
        fontFamily: Fonts.gothamMedium,
    },
    subTitleText: {
        textAlign: 'center',
        color: '#1c1f2c',
        fontSize: calcWidth(3.2),
        fontFamily: Fonts.gothamMedium,
    }
})