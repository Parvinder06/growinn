import { StyleSheet } from 'react-native';
import { calcHeight, calcWidth, calcHeightUsingPixel } from '../../utils/Dimensions'
import { Color } from '../../utils/Color';
import { Fonts } from '../../utils/Constants';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: Color.backgroundWhite
  },
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: Color.backgroundWhite
  },
  bgTop: {
    borderRadius: 10,
    marginTop: calcHeight(2),
    width: calcWidth(36)
  },
  title: {
    color: '#696969',
    fontSize: calcWidth(3.2),
    fontFamily: Fonts.gothamMedium,
  },
  subtTitleBlack: {
    color: '#171e31',
    fontSize: calcWidth(3.2),
    fontFamily: Fonts.gothamMedium,
    marginTop: 5
  },
  row: {
    flexDirection: 'row',
  },
  topView: {
    width: calcWidth(90),
    alignSelf: 'center',
  },
  videoItem: {
    height: calcHeight(22),
    width: calcWidth(90),
    borderRadius: calcWidth(2),
    alignSelf: 'center',
  },
  bgWhite: {
    width: calcWidth(90),
    margin: 10,
    backgroundColor: 'white',
    borderRadius: 10,
    borderColor: '#dddfec',
    borderWidth: 0.5,
    shadowOpacity: 0.1,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowColor: '#dddfec',
    elevation: 4,
    marginTop: calcHeight(2.5),
    padding:15,
    paddingVertical:20,
  },
  headerTitle: {
    color: '#1c1f2c',
    fontSize: calcWidth(4.8),
    fontFamily: Fonts.gothamBold,
  },
  HeaderSubtTitleBlack: {
    color: '#171e31',
    fontSize: calcWidth(3),
    fontFamily: Fonts.gothamMedium,
    marginTop: calcHeight(2),
    lineHeight:25
  },
  itemStyle:{
    flexDirection:'row',
  },
  itemTick:{
    color: '#171e31',
    fontSize: calcWidth(3),
    fontFamily: Fonts.gothamMedium,
    marginTop: calcHeight(2),
    marginRight:5
  },
  row: {
    flexDirection: 'row',
    width: calcWidth(90),
    alignSelf:'center',
    marginTop: calcHeight(2)
  },
  orangeView: {
    backgroundColor: Color.orangeText,
    padding: 5,
    paddingHorizontal: 8,
    borderRadius: 5,
    marginRight: calcWidth(3),
  },
  textWhite: {
    textAlign: 'center',
    color: 'white',
    fontSize: calcWidth(3),
    fontFamily: Fonts.gothamMedium,
  },
  textOrange: {
    color: Color.orangeText,
    fontSize: calcWidth(3),
    fontFamily: Fonts.gothamMedium,
    width: calcWidth(70),
  },
  floatingButton: {
    flexDirection: 'row',
    backgroundColor: Color.lightGreen,
    padding: 10,
    width: calcWidth(90),
    position: 'absolute',
    bottom: calcHeight(5),
    borderRadius: 5,
    justifyContent: 'space-between',
    alignItems: 'center',
    shadowOffset: { width: 5, height: 5 },
      shadowColor: Color.lightGreen,
      shadowOpacity: 0.2
  },
  addToCartText: {
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(3.5),
    color: 'white',
  },
  addToCartSubText: {
    fontFamily: Fonts.gothamBook,
    marginBottom: 5,
    fontSize: calcWidth(2.5),
    color: 'white',
    textDecorationLine: 'line-through'
  }
})