import { StyleSheet } from 'react-native';
import { calcHeight, calcWidth, calcHeightUsingPixel } from '../../../utils/Dimensions'
import { Color } from '../../../utils/Color';
import { Fonts } from '../../../utils/Constants';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: Color.backgroundWhite
  },
  textTopView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: calcWidth(90),
    marginTop: calcHeight(2.5)
  },
  textTop: {
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(3.2),
    color: Color.navyBlue
  },
  textTopRight: {
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(3.2),
    color: Color.navyBlue,
    marginRight: calcWidth(10)
  },
  lineView: {
    backgroundColor: Color.lineBlue,
    height: 1,
    width: calcWidth(90),
    marginVertical: calcHeight(1.5)
  },
  cartStyle: {
    margin: 5,
    borderRadius: 8,
    width: calcWidth(90)
  },
  cartText: {
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(3),
    color: Color.blackText
  },
  flatCartItemImage: {
    height: calcHeight(6.8),
    width: calcWidth(18),
    borderRadius: calcWidth(2),
    marginBottom: calcHeight(1)
  },
  flatListCart: {
    maxHeight: calcHeight(60)
  },
  flexRowItem: {
    flexDirection: 'row',
  },
  flexColumnItem: {
    marginHorizontal: calcWidth(4),
    width: calcWidth(40)
  },
  orangeView: {
    backgroundColor: 'rgba(243, 121, 44, 0.17)',
    borderRadius: 10,
    padding: 4,
    paddingHorizontal: 6,
    alignItems: 'center',
    justifyContent: 'center',
   // width: calcWidth(12)
  },
  orangeText: {
    color: Color.orangeText,
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(2.5),
  },
  titleText: {
    color: '#0a174c',
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(2.8),
    marginVertical: calcHeight(0.5),
    marginHorizontal: calcWidth(5),
  },
  authorText: {
    color: Color.textLightBlack,
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(2.5),
    width: calcWidth(30),
    marginHorizontal: calcWidth(5),
    lineHeight: 15,
    opacity: 0.61,
  },
  greenLineView: {
    borderRadius: 4,
    borderWidth: 0.8,
    borderColor: Color.lightGreen,
    padding: 10,
    width: calcWidth(90),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  checkOutText: {
    color: Color.lightGreen,
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(3),
  },
  afterDiscountText: {
    color: Color.lightGreen,
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(2),
    marginTop: 5,
    textDecorationLine: 'line-through'
  },
  bottomView: {
    paddingVertical: 10,
    width: calcWidth(90),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  viewText1: {
    color: Color.darkBlueText,
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(2.8),
  },
  viewText2: {
    color: Color.orangeText,
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(2.8),
  },
  flexRow: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  offerTitle: {
    color: Color.charcoal,
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(4.5),
  },
  viewModalWhite: {
    width: calcWidth(100),
    height: calcHeight(90),
    borderRadius: 20,
    backgroundColor: Color.backgroundWhite
  },
  viewModalTop: {
    width: calcWidth(100),
    height: calcHeight(8),
    borderTopLeftRadius: calcWidth(5),
    borderTopRightRadius: calcWidth(5),
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20
  },
  transparentView: {
    height: calcHeight(15),
    width: calcWidth(100),
    backgroundColor: 'rgba(0,0,0,0.3)'
  },
  inputTextView: {
    backgroundColor: 'white',
    marginVertical: calcHeight(2),
    width: calcWidth(90),
    borderRadius: calcWidth(1),
    borderColor: '#dde0f0',
    fontFamily: Fonts.gothamMedium,
    borderWidth: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    alignSelf: 'center',
    flexDirection: 'row',
    paddingVertical: 5
  },
  inputText: {
    width: calcWidth(65),
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(3.5),
    marginLeft: 20,
    marginVertical: calcHeight(1),
    color: '#1c1f2c',
  },
  inputTextApply: {
    width: calcWidth(70),
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(3.5),
    //alignSelf: 'flex-start',
    marginLeft: 20,
    marginVertical: calcHeight(1),
    color: Color.orangeText,
  },
  specialView: {
    backgroundColor: 'white',
    //width: calcWidth(100),
    padding: calcWidth(5)
  },
  specialText: {
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(3.8),
    color: Color.charcoal,
  },
  offerSubText: {
    color: Color.darkBlueText,
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(2.8),
    width: calcWidth(70),
    marginTop: calcWidth(2),
    lineHeight: 15,
    opacity: 0.47,
  },
  orangeOfferView: {
    backgroundColor: 'rgba(243, 121, 44, 0.17)',
    borderRadius: 2,
    borderStyle:'dashed',
    borderWidth:0.8,
    borderColor: Color.orangeText,
    padding: 4,
    paddingHorizontal: 6,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop:5,
    width: 'auto',
  },
  orangeOfferText: {
    color: Color.orangeText,
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(2.5),
  },
})