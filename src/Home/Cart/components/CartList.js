import React, { Component, useState, useEffect } from 'react';
import { View, Image, FlatList, TouchableOpacity, Text, StatusBar, BackHandler, Linking, Platform } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { styles } from '../Styles';
import { GET_CATEGORIES } from '../../../apollo/query/Home'
import { useQuery } from '@apollo/client';
import FastImage from 'react-native-fast-image';

const DATA = [
    {
        image: 'https://graphql-growinn.s3.ap-south-1.amazonaws.com/categories/Art.png',
        name: 'Krumping'
    },
    {
        image: 'https://graphql-growinn.s3.ap-south-1.amazonaws.com/categories/Art.png',
        name: 'Dance'
    },
]
export const CartList = (props) => {
    // const { loading, error, data } = useQuery(GET_CATEGORIES)

    const renderCartItem = (item, index) => {
        return (
            <TouchableOpacity onPress={() =>props.onPressItem()}
                style={styles.cartStyle}>
                <View style={styles.flexRowItem}>
                    <FastImage style={styles.flatCartItemImage}
                        source={{
                            uri: item.image,
                            priority: FastImage.priority.high,
                        }}>
                    </FastImage>
                    <View>
                        <View style={styles.flexColumnItem}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={styles.orangeView}>
                                    <Text style={styles.orangeText}>Dance</Text>
                                </View>
                            </View>
                        </View>
                        <View>
                            <Text style={styles.titleText}>{item.name}</Text>
                            <Text style={styles.authorText}>By Jason Will • Duration 3 month</Text>
                        </View>
                    </View>
                    <Text style={styles.titleText}>{'Rs.2100'}</Text>
                </View>
                <View style={styles.lineView} />
            </TouchableOpacity>
        );
    }

    const renderCartList = () => {
        return <FlatList
            style={styles.flatListCart}
            data={DATA}
            renderItem={({ item, index }) => renderCartItem(item, index)} />
    }

    return (
        <View>
            {renderCartList()}
        </View>
    );

};

export default CartList;
