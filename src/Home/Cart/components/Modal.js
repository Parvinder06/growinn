import React, { Component, useState, useEffect } from 'react';
import { View, Image, FlatList, TouchableOpacity, Text, StatusBar, TextInput, Modal, Platform } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { styles } from '../Styles';
import { GET_CATEGORIES } from '../../../apollo/query/Home'
import { useQuery } from '@apollo/client';
import FastImage from 'react-native-fast-image';
import { calcHeight, calcWidth } from '../../../utils/Dimensions'
import { Color } from '../../../utils/Color';
import CrossIc from '../../../assets/images/icCloseDark.svgx';

const DATA = [
    {
        image: 'https://graphql-growinn.s3.ap-south-1.amazonaws.com/categories/Art.png',
        name: 'Krumping'
    },
    {
        image: 'https://graphql-growinn.s3.ap-south-1.amazonaws.com/categories/Art.png',
        name: 'Dance'
    },
]
export const ModalView = (props) => {
    const [offerText, setOfferText] = useState('')
    // const { loading, error, data } = useQuery(GET_CATEGORIES)

    const setOfferTextFn = (value) => {
        setOfferText(value)
    }

    return (
        <Modal
            style={{
                flex: 1,
                height: calcHeight(100),
                width: calcWidth(100),
            }}
            swipeDirection={['down']}
            transparent={true}
            isVisible={props.visible}>
            <TouchableOpacity onPress={props.onHide}
                style={styles.transparentView}
            />

            <View style={{ backgroundColor: 'rgba(0,0,0,0.3)' }}>
                <View style={styles.viewModalWhite}>
                    <View style={styles.viewModalTop}>
                        <Text style={styles.offerTitle}>Check your offers</Text>
                        <TouchableOpacity onPress={props.onHide}>
                            <CrossIc height={20} width={20} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.inputTextView}>
                        <TextInput style={styles.inputText}
                            value={offerText}
                            onChangeText={value => setOfferTextFn(value)}
                        />
                        <Text style={styles.inputTextApply}>Apply</Text>
                    </View>
                    <View style={styles.specialView}>
                        <Text style={styles.specialText}>Special offer</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={styles.orangeOfferView}>

                                <Text style={styles.orangeOfferText}>GROWIWIN</Text>

                            </View>
                        </View>
                        <Text style={styles.offerSubText}>Use coupan GROWINN to avial extra 10%off,
                            offer expire soon</Text>
                    </View>
                </View>
            </View>
        </Modal >
    );

};

export default ModalView;
