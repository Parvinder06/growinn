import React, { Component, useState, useEffect } from 'react';
import { View, Image, FlatList, TouchableOpacity, Text, StatusBar, BackHandler, Linking, Platform } from 'react-native';
import { calcHeight, calcWidth } from '../../utils/Dimensions'
import { Actions } from 'react-native-router-flux';
import { styles } from './Styles';
import LineButton from '../../components/Button/LineButton';
import FastImage from 'react-native-fast-image';
import { BackHeader } from '../../components/BackHeader/blackHeader';
import { ScrollView } from 'react-native';
import { GET_CART } from '../../apollo/query/Cart'
import { useQuery, useLazyQuery } from '@apollo/client';
import CartList from './components/CartList';
import Icon from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import AsyncStorage from '@react-native-community/async-storage';
import { Color } from '../../utils/Color';
import Modal from './components/Modal';

export const CartScreen = (props) => {

    let token = AsyncStorage.getItem('Token_Key', '')
    const [selectedId, setSelectedId] = useState(null);
    const [selectedStart, setSelectedStart] = useState(null);
    const [visible, setVisible] = useState(false);
    const [refetch] = useLazyQuery(GET_CART,
        {
            onCompleted: data => { console.log('on completed', data); },
            fetchPolicy: 'network-only',
            notifyOnNetworkStatusChange: true
        });

    useEffect(() => {
        const { navigation } = props;
        const isFocused = navigation.isFocused();
        const navFocusListener = navigation.addListener('didFocus', () => {
            refetch()
        });

        return () => {
            navFocusListener.remove();
        };
    }, []);

    const handleOnEndReached = () => {
        return refetch()
    };


    return (
        <View style={styles.container}>
            <BackHeader backDisabled={props?.backDisabled} props={props} title={'Cart'} />
            <View style={styles.textTopView}>
                <Text style={styles.textTop}>Product</Text>
                <Text style={styles.textTopRight}>Price</Text>
            </View>
            <View style={styles.lineView} />
            <CartList onPressItem={() => handleOnEndReached()} />
            <View style={styles.greenLineView}>
                <Text style={styles.checkOutText}>Check Out</Text>
                <View>
                    <Text style={styles.checkOutText}>Rs.1800</Text>
                    <Text style={styles.afterDiscountText}>Rs.2100</Text>
                </View>
            </View>
            <View style={styles.bottomView}>
                <View style={styles.flexRow}>
                    <Icon
                        name='tag'
                        size={20}
                        color={Color.orangeText}
                        style={{ marginRight: 10, alignSelf: 'center' }}
                    />
                    <Text style={styles.viewText1}>Apply Coupan</Text>
                </View>
                <TouchableOpacity onPress={() => setVisible(true)} style={styles.flexRow}>
                    <Text style={styles.viewText2}>View Offers</Text>
                    <Entypo
                        //name='chevron-small-right'
                        name='chevron-right'
                        size={20}
                        color={Color.orangeText}
                        style={{ alignSelf: 'center' }}
                    />
                </TouchableOpacity>
            </View>
            {visible ? <Modal visible={visible} onHide={() => setVisible(false)} onApply={() => setVisible(false)} /> : null}
        </View>
    );

};

export default CartScreen;
