import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import Video from 'react-native-video';
// importing the dependency didn't work in the snack so I just copied the file and accessed it manually
import InViewPort from './InViewPort';

export default class VideoPlayer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            paused: false
        }
    }

    pauseVideo = () => {
        if (this.video) {
            this.setState({
                paused : true
            });
        }
        console.log('pauseVideo')

    }

    playVideo = () => {
        if (this.video) {
            this.setState({
                paused : false
            });
            console.log('playVideo')
        }
    }

    handlePlaying = (isVisible) => {
        isVisible ? this.playVideo() : this.pauseVideo();
    }

    render() {
        return (
            <View style={styles.container}>
                <InViewPort onChange={this.handlePlaying}>
                    <Video
                        ref={ref => { this.video = ref }}
                        source={{ uri: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4' }}
                        rate={1.0}
                        volume={1.0}
                        isMuted={false}
                        resizeMode="cover"
                        shouldPlay
                        style={{ width: 300, height: 300 }}
                        controls={true}
                        paused={this.state.paused}
                    />
                </InViewPort>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center'
    }
});