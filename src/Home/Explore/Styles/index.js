import { StyleSheet } from 'react-native';
import { calcHeight, calcWidth, calcHeightUsingPixel } from '../../../utils/Dimensions'
import { Color } from '../../../utils/Color';
import { Fonts } from '../../../utils/Constants';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white'
  },
  topBar: {
    width: calcWidth(100),
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    height: calcHeight(8.49),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'center',
    backgroundColor: Color.white,
    padding: 18
  },
  scroll: {
    // marginTop: calcHeight(12),
  },
  topPurpleView: {
    width: calcWidth(100),
    height: calcHeight(13),
    //  marginTop: calcHeight(8),
    backgroundColor: 'rgb(42,63,141)',
    justifyContent: 'center',
    alignItems: 'center'
  },

  inputTextView: {
    backgroundColor: Color.backgroundText,
    marginVertical: calcHeight(1),
    width: calcWidth(90),
    borderRadius: calcWidth(1),
    borderColor: Color.silver,
    fontFamily: Fonts.interRegular,
    fontSize: calcWidth(4),
    padding: calcWidth(4),
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  upcomingText: {
    width: calcWidth(80),
    fontFamily: Fonts.sfMedium,
    fontSize: calcWidth(4.8),
    color: Color.blackText
  },
  seeMore: {
    width: calcWidth(80),
    fontFamily: Fonts.sfMedium,
    fontSize: calcWidth(3.5),
    color: Color.darkGreyText
  },
  LoginView: {
    backgroundColor: Color.darkGrey,
    height: calcHeight(6),
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: calcHeight(1.5),
    width: calcWidth(90),
    borderRadius: calcWidth(1),
    flexDirection: 'row',
  },
  LoginText: {
    textAlign: 'center',
    paddingHorizontal: 15,
    color: Color.white,
    fontSize: calcWidth(4),
    fontFamily: Fonts.interSemiBold,
    width: calcWidth(70),
  },
  forgotText: {
    textAlign: 'center',
    color: Color.skyBlue,
    fontSize: calcWidth(3.5),
    fontFamily: Fonts.interSemiBold,
  },
  bottom: {
    position: 'absolute',
    bottom: calcHeight(2),
    flexDirection: 'row',
    justifyContent: 'center',
  },
  continueText: {
    textAlign: 'center',
    color: Color.darkGreyText,
    fontSize: calcWidth(3),
    fontFamily: Fonts.interRegular,
    width: calcWidth(70),
    margin: calcHeight(3)
  },
  labelText: {
    textAlign: 'center',
    color: Color.white,
    fontSize: calcWidth(4.5),
    fontFamily: Fonts.sfMedium,
    width: calcWidth(100),
  },
  labelSubText: {
    color: Color.darkGreyText,
    fontSize: calcWidth(3.5),
    fontFamily: Fonts.sfRegular,
    margin: 10,
    marginVertical: calcHeight(1),
    marginHorizontal: calcWidth(4),
  },
  catStyle: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    margin: 5,
    backgroundColor: Color.green,
    paddingHorizontal: calcWidth(3.5),
    paddingVertical: calcHeight(1.6),
    borderRadius: 8,
  },
  catText: {
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(3),
    color: Color.white
  },
  courseText: {
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(3),
    color: Color.charcoal
  },
  courseSubText: {
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(2),
    color: Color.charcoal,
    marginTop: calcHeight(1)
  },
  trialStyle: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginVertical: calcHeight(2),
    height: calcHeight(12),
    width: calcWidth(80),
    backgroundColor: Color.green,
    paddingHorizontal: calcWidth(3.5),
    paddingVertical: calcHeight(1.6),
    borderRadius: calcWidth(4),
  },
  suggestedParent: {
    marginHorizontal: calcWidth(2),
   // height: calcHeight(),
    width: calcWidth(60),
  },
  viewAllCatText: {
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(3.5),
    color: Color.charcoal
  },
  blackBg: {
    backgroundColor: Color.charcoal,
    flex: 1,
    borderTopLeftRadius: calcWidth(10),
    borderTopRightRadius: calcWidth(10)
  },
  toggleText: {
    fontFamily: Fonts.sfMedium,
    fontSize: calcWidth(3.5),
    color: Color.darkGreyText,
    textAlign: 'center'
  },
  toggleView: {
    justifyContent: 'center',
    marginTop: calcHeight(1) + 10,
    width: calcWidth(25),
    alignItems: 'center'
    // paddingHorizontal: 15,
    // paddingVertical: 10,
    //  borderBottomWidth: 0.5,
  },
  titleText: {
    marginTop: calcHeight(1),
    fontFamily: Fonts.sfMedium,
    fontSize: calcWidth(4),
    textAlign: 'left',
    color: Color.blackText,
  },
  lineView: {
    width: calcWidth(60),
    height: calcHeight(0.1),
    marginVertical: calcHeight(1),
    backgroundColor: Color.silver,
    alignSelf: 'center'
  },
  topView: {
    shadowOpacity: 0.1,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowColor: '#000000',
    elevation: 4,
    flexDirection: 'row',
    width: calcWidth(100),
    height: calcHeight(7),
    backgroundColor: Color.white,
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  flatCourseItem :{
    height: calcHeight(14),
    width: calcWidth(60),
    borderRadius: calcWidth(4),    
    marginBottom: calcHeight(1)   
  },
  bannerItem :{
    height: calcHeight(12),
    width: calcWidth(90),
    borderRadius: calcWidth(4),    
    marginBottom: calcHeight(1),
    alignSelf:'center'
  },
  videoItem :{
    height: calcHeight(20),
    width: calcWidth(90),
    borderRadius: calcWidth(4),    
    alignSelf:'center',
  },
  popularSubTitle: {
    fontFamily: Fonts.gothamMedium,
    fontSize: calcWidth(2.5),
    color: Color.charcoal,
    marginTop: 5
  },
  popularTopView :{
    backgroundColor: Color.transparentGreen,
    marginLeft: calcWidth(5), marginBottom: calcHeight(1.5),
    width: calcWidth(90),
    padding: 10,
    borderRadius:8
  },
  catTitle: {
    color:Color.charcoal,
    fontFamily: Fonts.gothamBold,
    fontSize: calcWidth(4),
    position:'absolute',
    bottom:10,
    left: 10
  },
  bgCat: {
    width:calcWidth(40),
    height: calcHeight(13),
    margin: 10,
    marginVertical: calcHeight(1.2),
    borderRadius:10,
    shadowOpacity: 0.1,
    shadowOffset: {
      width: 5,
      height: 5,
    },
    shadowColor: '#000000',
    elevation: 4,
  },
  bgInnerCat:{
    width:calcWidth(40),
    height: calcHeight(13),
    borderRadius:10,
  },
})