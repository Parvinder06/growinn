import React, { Component, useState, useEffect } from 'react';
import { View, Image, FlatList, TouchableOpacity, Text, StatusBar, BackHandler, Linking, Platform } from 'react-native';
import { calcHeight, calcWidth } from '../../../utils/Dimensions'
import { Actions } from 'react-native-router-flux';
import { styles } from './../Styles';
import { Color } from '../../../utils/Color';
import DataManager from '../../../../src/database/DataManager';
import { GET_CATEGORIES } from '../../../apollo/query/Home'
import { useQuery } from '@apollo/client';
const colorArrayConst = ['#208354', '#f3792c', '#208354', '#2973cc', '#382d8b', '#fa94d3', '#036082', '#2cf389', '#fa94d3'];

export const Categories = (props) => {
    //const [catData, setTrialData] = useState(props.data);
    const [colorArray, setColorArray] = useState(colorArrayConst);
    const { loading, error, data } = useQuery(GET_CATEGORIES)

    const getColor = () => {
        let number = Math.floor(Math.random() * 8);
        return colorArray[number]
    }

    const renderCatItem = (item, index) => {
        return (
            <TouchableOpacity onPress={() => { }}
                style={[styles.catStyle, { marginLeft: index === 0 ? calcWidth(5) : null, backgroundColor: getColor() }]}>
                <Text style={styles.catText}>{item.name}</Text>
            </TouchableOpacity>
        );
    }

    const renderCategories = () => {
        return <FlatList
            style={{ marginTop: calcHeight(1), }}
            data={data?.categories}
            horizontal={true}
            renderItem={({ item, index }) => renderCatItem(item, index)} />
    }

    viewAll = () => {
        return (
            <TouchableOpacity onPress={() => { }}
                style={{ alignSelf: 'center', flexDirection: 'row', marginVertical: calcHeight(1.2) }}>
                <Text style={styles.viewAllCatText}>{`View all categories`}</Text>
            </TouchableOpacity>
        );
    }

    return (
        <View>
            {renderCategories()}
        </View>
    );

};

export default Categories;
