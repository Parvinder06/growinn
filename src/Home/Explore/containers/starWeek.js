import React, { Component, useState, useEffect } from 'react';
import { View, Image, FlatList, TouchableOpacity, Text, StatusBar, BackHandler, Linking, Platform } from 'react-native';
import { calcHeight, calcWidth } from '../../../utils/Dimensions'
import { Actions } from 'react-native-router-flux';
import { styles } from './../Styles';
import { Color } from '../../../utils/Color';
import DataManager from '../../../../src/database/DataManager';
import { connect } from 'react-redux';
import LineButton from '../../../components/Button/LineButton';
import VideoPlayer from './../components/VideoPlayer';
import FastImage from 'react-native-fast-image';
import Video from 'react-native-video';
import IcPlay from '../../../assets/images/playIc.svgx';

const url = 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4';
export const StarWeek = (props) => {
    const [triealData, setTrialData] = useState(props.data);
    const [pausedValue, setPaused] = useState(props.pausedValue);

    useEffect(() => {
        return () => {
            setPaused(true)
        }
    }, []); 

    useEffect(() => {
        const { navigation } = props;
        const navFocusListener = navigation.addListener('didBlur', () => {
            setPaused(true)
            console.log(' useEffect setPaused(true) ')
        });

        return () => {
            navFocusListener.remove();
        };
    }, []);

    const renderVideo = () => {
        return <TouchableOpacity onPress={() => setPaused(!pausedValue)}>
            <Video source={{ uri: url }}
                style={styles.videoItem}
                ref={ref => {
                    this.player = ref;
                }}

                //onButtonClick={() => setPaused(!pausedValue)}
               // controls={true}
                paused={pausedValue}
                resizeMode={'cover'}
                playInBackground={false}
                playWhenInactive={false}
                hideShutterView={true}
                // poster={this.state.thumbnailUrl}
                posterResizeMode={'cover'}
            />
        </TouchableOpacity>
    }
    const renderGetCourse = () => {
        return (
            <LineButton
                buttonText={'Book Trial'}
                width={calcWidth(90)}
                height={calcHeight(4.5)}
                pressed={() => {
                    Actions.ageScreen()
                }}
            />
        )
    }

    const renderPlay = () => {
        return (
            <IcPlay />
        )
    }

    const newText = () => {
        return (
            <View onPress={() => { }}
                style={{ marginLeft: calcWidth(5), marginBottom: calcHeight(1.5) }}>
                <Text style={styles.viewAllCatText}>{`Star of the week`}</Text>
            </View>
        );
    }


    return (
        <View style={{ marginBottom: calcHeight(3) }}>
            {newText()}
            <View>
                {renderVideo()}
                {pausedValue ? <TouchableOpacity onPress={() => setPaused(!pausedValue)}
                    style={{ position: 'absolute', alignSelf: 'center', marginTop: calcHeight(7) }}>
                    {renderPlay()}
                </TouchableOpacity> : null}
            </View>
            {renderGetCourse()}
        </View >
    );

};

export default StarWeek;
