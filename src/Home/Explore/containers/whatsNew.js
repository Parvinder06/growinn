import React, { Component, useState, useEffect } from 'react';
import { View, Image, FlatList, TouchableOpacity, Text, StatusBar, BackHandler, Linking, Platform } from 'react-native';
import { calcHeight, calcWidth } from '../../../utils/Dimensions'
import { Actions } from 'react-native-router-flux';
import { styles } from './../Styles';
import { Color } from '../../../utils/Color';
import DataManager from '../../../../src/database/DataManager';
import { connect } from 'react-redux';
import LineButton from '../../../components/Button/LineButton';
import Carousel, { Pagination } from 'react-native-snap-carousel';
const colorArrayConst = ['#208354', '#f3792c', '#208354', '#2973cc', '#382d8b', '#fa94d3', '#036082', '#2cf389', '#fa94d3'];

export const WhatsNew = (props) => {
    const [trialData, setTrialData] = useState(props.data);
    const [colorArray, setColorArray] = useState(colorArrayConst);

    const getColor = () => {
        let number = Math.floor(Math.random() * 8);
        return colorArray[number]
    }

    const renderCatItem = (item, index) => {
        return (
            <TouchableOpacity onPress={() => { }}
                style={[styles.trialStyle, {  backgroundColor: getColor() }]}>
                <Text style={styles.catText}>{item.title}</Text>
            </TouchableOpacity>
        );
    }

    const renderCategories = () => {
        return <Carousel
            ref={(c) => { this._carousel = c; }}
            data={trialData}
            renderItem={renderCatItem}
            sliderWidth={calcWidth(100)}
            itemWidth={calcWidth(80)}
            dotColor={Color.appColor}
            activeAnimationType={'spring'}
            activeAnimationOptions={{
                friction: 4,
                tension: 40
            }}
        />

    }

    const newText = () => {
        return (
            <View onPress={() => { }}
                style={{ marginLeft: calcWidth(5), marginBottom: 1 }}>
                <Text style={styles.viewAllCatText}>{`What's new`}</Text>
            </View>
        );
    }

    const renderButton = () => {
        return (
            <LineButton
                buttonText={'Book Trial'}
                width={calcWidth(90)}
                height={calcHeight(4.5)}
                marginTop={-1}
                regular
                pressed={() => {
                    Actions.ageScreen()
                }}
            />
        )
    }

    return (
        <View style={{ backgroundColor: Color.lightGreyBg, marginTop: calcHeight(2), paddingVertical: calcHeight(2.5) }}>
            {newText()}
            {renderCategories()}
            {renderButton()}
        </View>
    );
}