import React, { Component, useState, useEffect } from 'react';
import { View, ScrollView, FlatList, TouchableOpacity, Text, StatusBar, BackHandler, Linking, Platform } from 'react-native';
import { calcHeight, calcWidth } from '../../../utils/Dimensions'
import { Actions } from 'react-native-router-flux';
import { styles } from './../Styles';
import { Color } from '../../../utils/Color';
import DataManager from '../../../../src/database/DataManager';
import { connect } from 'react-redux';
import LineButton from '../../../components/Button/LineButton';
import VideoPlayer from './../components/VideoPlayer';
import FastImage from 'react-native-fast-image';
import Video from 'react-native-video';
import IcPlay from '../../../assets/images/playIc.svgx';

const url = 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4';
export const PopularCategory = (props) => {
    const [triealData, setTrialData] = useState(props.data);
    const [pausedValue, setPaused] = useState(true);
    const renderVideo = () => {
        return <TouchableOpacity onPress={() => setPaused(!pausedValue)}>
            <Video source={{ uri: url }}
                style={styles.videoItem}
                ref={ref => {
                    this.player = ref;
                }}

                //onButtonClick={() => setPaused(!pausedValue)}
                //controls={true}
                paused={pausedValue}
                resizeMode={'cover'}
                playInBackground={false}
                playWhenInactive={false}
                hideShutterView={true}
                // poster={this.state.thumbnailUrl}
                posterResizeMode={'cover'}
            />
        </TouchableOpacity>
    }
    const renderGetCourse = () => {
        return (
            <LineButton
                buttonText={'Book Trial'}
                width={calcWidth(90)}
                height={calcHeight(4.5)}
                pressed={() => {
                    Actions.ageScreen()
                }}
            />
        )
    }

    const renderPlay = () => {
        return (
            <IcPlay />
        )
    }

    const newText = () => {
        return (
            <View
                style={styles.popularTopView}>
                <Text style={styles.viewAllCatText}>{`Most popular category`}</Text>
                <Text style={styles.popularSubTitle}>{`Lorem ipsum dolor sit amet, consec tetur adipiscing eli`}</Text>
            </View>
        );
    }

    const selectItem =(item) =>{

    }

  /*   const renderCatItem = (item, index) => {
        return (
            <TouchableOpacity onPress={() => { }}
                style={[styles.catStyle, { marginLeft: index === 0 ? calcWidth(5) : null }]}>
                <Text style={styles.catText}>{item.title}</Text>
            </TouchableOpacity>
        );
    } */
    const renderCatItem = (item, index) => {
        return (
            <TouchableOpacity style={styles.bgCat} onPress={() => selectItem(item)}>
                <FastImage style={styles.bgInnerCat}
                    source={{
                        uri: 'https://unsplash.it/400/400?image=1',
                        priority: FastImage.priority.high,
                    }}>
                </FastImage>
                <Text style={styles.catTitle}>{'Dance'}</Text>
            </TouchableOpacity>
        );
    }

    const renderCategories = () => {
        return  <FlatList
                scrollEnabled={false}
                numColumns={2}
                data={triealData}
                renderItem={({ item, index }) => renderCatItem(item, index)} />
    }
    return (
        <View style={{ paddingBottom: calcHeight(5), backgroundColor: 'white', }}>
            {newText()}
            <View style ={{width: calcWidth(90), alignSelf:'center'}}>
                {renderCategories()}
            </View>
            {renderGetCourse()}
        </View >
    );

};

export default PopularCategory;
