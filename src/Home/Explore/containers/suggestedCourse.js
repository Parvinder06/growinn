import React, { Component, useState, useEffect } from 'react';
import { View, Image, FlatList, TouchableOpacity, Text, StatusBar, BackHandler, Linking, Platform } from 'react-native';
import { calcHeight, calcWidth } from '../../../utils/Dimensions'
import { Actions } from 'react-native-router-flux';
import { styles } from './../Styles';
import { Color } from '../../../utils/Color';
import DataManager from '../../../../src/database/DataManager';
import { connect } from 'react-redux';
import LineButton from '../../../components/Button/LineButton';
import VideoPlayer from './../components/VideoPlayer';
import FastImage from 'react-native-fast-image';
import { GET_COURSECONNECTION } from '../../../apollo/query/Home'
import { useQuery, useLazyQuery, NetworkStatus } from '@apollo/client';
export const SuggestedCourse = (props) => {
    //const [cData, setData] = useState([]);
    const [count, setCount] = useState(0);
    const [fetchedData, setFetechedData] = useState([])
    const { data, error, loading, fetchMore, refetch, networkStatus } = useQuery(
        GET_COURSECONNECTION,
        {
          variables: { first: 2 },
        }
      );

      let cData = [];
      cData = data?.coursesConnection?.edges;
    
      const onUpdate = (prev, { fetchMoreResult }) => {
        if (!fetchMoreResult) return prev;
        const { pageInfo } = fetchMoreResult.coursesConnection;
        const edges = [
          ...prev.coursesConnection?.edges,
          ...fetchMoreResult.coursesConnection?.edges,
        ];
       return Object.assign({}, prev, {
            coursesConnection: {
            __typename: prev.coursesConnection.__typename,
            pageInfo,
            edges
        /*     edges: {
              __typename: prev.coursesConnection.edges.__typename,
              edges,
              node:{
                __typename: prev.coursesConnection.edges.node__typename,
              }
            }, */
          },
        });
      };
    //   const { loading, error, data } = useQuery(GET_COURSECONNECTION)
/*     const [getCourseData, loading] = useLazyQuery(GET_COURSECONNECTION, {
        onError: (err) => {
            console.log(JSON.stringify(err))
            alert(JSON.stringify(err))
            //  setIsLoading(false)
        },
        onCompleted: (data) => {
            console.log(JSON.stringify(data))
            //setIsLoading(false)
            alert('j')
            let combine = [...cData, ...data?.coursesConnection?.edges,]
            setData(combine)
        }
    });

    if (loading) debugger;


    useEffect(() => {
        console.log('useEffect');
        getCourseData
            ({
                variables:
                {
                    first: 2,
                }
            }
            );
        debugger;
    }, [count]);
 */
    /*     useEffect(() => {
            cData && setFetechedData([...fetchedData, cData])
        }, [cData]); */

 /*    const moreData = () => {
        setCount(count + 1)
        alert(count)
        getCourseData
            ({
                variables:
                {
                    first: 2,
                }
            }
            );
    }
 */
    const handleOnEndReached = () => {
        if (data.coursesConnection.pageInfo.hasNextPage)
          return fetchMore({
            variables: {
              after: data.coursesConnection.pageInfo.endCursor,
              first: 2,
            },
            updateQuery: onUpdate,
          });
      };

    const renderItem = (item, index) => {
        return (
            <View style={[styles.suggestedParent, { marginLeft: index === 0 ? calcWidth(5) : null }]}>
                <FastImage style={styles.flatCourseItem}
                    source={{
                        uri: item?.node?.thumbnail,
                        priority: FastImage.priority.high,
                    }}>
                </FastImage>
                <Text style={styles.courseText}>{item?.node.name}</Text>
                <Text style={styles.courseSubText}>{`By ${item?.node?.teacher.name} • Duration 3 mon`}</Text>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                    {renderButtonDemo(item)}
                    {renderGetCourse(item)}
                </View>
            </View>
        );
    }

    const renderCategories = () => {
        return <FlatList
            ref={(c) => { this.flatListRef = c; }}
            style={{ marginBottom: 1 }}
            data={cData}
            horizontal={true}
            onEndReachedThreshold={0}
            onEndReached={(info) => {
                handleOnEndReached()
            }} 
/* 
            onEndReached={() => {
                fetchMore({
                  variables: {  after: data.coursesConnection.pageInfo.endCursor ,first: 2},
                  updateQuery: (previousResult, { fetchMoreResult }) => {
                    if (!fetchMoreResult || fetchMoreResult.coursesConnection.edges.length === 0) {
                      return previousResult;
                    }
                    alert(JSON.stringify(fetchMoreResult))

                    return {
                        data: [...previousResult.coursesConnection.edges , ...fetchMoreResult.coursesConnection.edges],
                    };
                  },
                });
              }} */

            renderItem={({ item, index }) => renderItem(item, index)} />

    }

    const newText = () => {
        return (
            <View onPress={() => { }}
                style={{ marginLeft: calcWidth(5), marginBottom: calcHeight(1.5) }}>
                <Text style={styles.viewAllCatText}>{`Suggested for you`}</Text>
            </View>
        );
    }

    const renderButtonDemo = (item) => {
        return (
            <LineButton
                buttonText={'Book Demo'}
                width={calcWidth(29)}
                height={calcHeight(3.5)}
                line
                pressed={() => {
                    Actions.demo({
                        data: item?.node
                    })
                }}
            />
        )
    }

    const renderGetCourse = (item) => {
        return (
            <LineButton
                buttonText={'Get Course'}
                width={calcWidth(29)}
                height={calcHeight(3.5)}
                regular
                pressed={() => {
                    Actions.course({
                        data: item?.node
                    })
                }}
            />
        )
    }

    return (
        <View style={{ paddingVertical: calcHeight(2.5) }}>
            {newText()}
            {renderCategories()}
        </View>
    );

};

export default SuggestedCourse;
