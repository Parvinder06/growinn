import React, { Component, useState, useEffect } from 'react';
import { View, Image, FlatList, TouchableOpacity, Text, StatusBar, BackHandler, Linking, Platform } from 'react-native';
import { calcHeight, calcWidth } from '../../../utils/Dimensions'
import { Actions } from 'react-native-router-flux';
import { styles } from './../Styles';
import { Color } from '../../../utils/Color';
import DataManager from '../../../../src/database/DataManager';
import { connect } from 'react-redux';
import LineButton from '../../../components/Button/LineButton';
import VideoPlayer from './../components/VideoPlayer';
import FastImage from 'react-native-fast-image';

export const BannerItem = (props) => {
    const [triealData, setTrialData] = useState(props.data);

    const renderCategories = () => {
        return <>
            <FastImage style={styles.bannerItem}
                source={{
                    uri: 'https://unsplash.it/400/400?image=1',
                    priority: FastImage.priority.high,
                }}>
            </FastImage>
        </>
    }
    const renderGetCourse = () => {
        return (
            <LineButton
                buttonText={'Book Trial'}
                width={calcWidth(29)}
                height={calcHeight(3.5)}
                line
                pressed={() => {
                    Actions.ageScreen()
                }}
            />
        )
    }

    return (
        <View style={[styles.bannerItem, {marginBottom: calcHeight(3)}]}>
            {renderCategories()}
            <View style ={{position:'absolute', bottom: 10, left: 10}}>
                {renderGetCourse()}
            </View>
        </View>
    );

};

export default BannerItem;
