/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

 import React, { Component } from 'react';
 import { Provider } from 'react-redux';
 import AppComponent from './AppComponent';
 import DataManager from './src/database/DataManager';
 import store from './src/store/store';
 import apolloClient from './src/apollo';
import { ApolloProvider } from '@apollo/client';
 class App extends Component {
 
   constructor(props) {
     super(props);
     this.state = {
       welcome: false,
       mobile: false,
       setup: false,
       home: false,
       client: apolloClient(),

     };
     DataManager.getInitialScreen().then((res) => {
       this.checkStatus(res)
     });

   }
   checkStatus = (param) => {
 
     switch (param) {
       case 'welcome':
         this.setState({ welcome: true })
         break;
       case 'mobile':
         this.setState({ mobile: true })
         break;
       case 'setup':
         this.setState({ setup: true })
         break;
       case 'home':
         this.setState({ home: true })
         break;
       default:
         this.setState({ welcome: true })
     }
 
   }

   setClient =  () =>{
  // let token = await AsyncStorage.getItem('Token_Key','')
   return apolloClient()
   }
 
 
   render() {
     return (
      <ApolloProvider client={this.setClient()}>
       <Provider store={store}>
         <AppComponent
           welcome={this.state.welcome}
           mobile={this.state.mobile}
           setup={this.state.setup}
           home={this.state.home}
         />
       </Provider>
       </ApolloProvider>
     );
   }
 };
 
 export default (App);
 
 