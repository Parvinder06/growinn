import React, { Component } from 'react';
import { View, StyleSheet, StatusBar, Animated, Easing, Platform } from 'react-native';
import { connect } from 'react-redux';
import RouterComponent from './Router'
import ToastView from './src/components/ToastViewComponent/ToastView';
import { Color } from './src/utils/Color'
import NetInfo from "@react-native-community/netinfo";

class AppComponent extends Component {

  state = {
    scale: new Animated.Value(1)
  }

  componentDidMount() {
   
  }

  componentDidUpdate() {
    if (this.props.isVisible) {
      Animated.timing(this.state.scale, {
        toValue: 0.95,
        duration: 300,
        useNativeDriver: true,
        easing: Easing.in()
      }).start();
    } else {
      Animated.timing(this.state.scale, {
        toValue: 1,
        duration: 300,
        useNativeDriver: true,
        easing: Easing.in()
      }).start();
    }
  }

  render() {
    return (
      <Animated.View style={{ flex: 1, transform: [{ scale: this.state.scale }] }}>
        <View style={styles.container}>
         {Platform.OS === 'android' ? <StatusBar
            barStyle="dark-content"
            // dark-content, light-content and default
            hidden={false}
            //To hide statusBar
            backgroundColor={Color.appThemeColor}
            //Background color of statusBar only works for Android
            translucent={false}
            //allowing light, but not detailed shapes
            networkActivityIndicatorVisible={true}
          /> : null }
          <RouterComponent
            welcome={this.props.welcome}
            mobile={this.props.mobile}
            setup={this.props.setup}
            home={this.props.home}
          />
          <ToastView
            toastColor='red'
          />
        </View>
      </Animated.View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  actionSheetOption: {
    color: 'black',
    fontSize: 18
  },
  actionSheetText: {
    color: 'black'
  }
})

export default AppComponent;
